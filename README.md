# Xblast
## Second semester OOP project
This repo is the code base for our second semester [OOP class](https://cs108.epfl.ch) project. We will write our own version of the famous [Xblast](http://xblast.sourceforge.net/) 

## The workflow
### External resources
It is highly recomended to have read [this](https://guides.github.com/introduction/flow/index.html), and to make it easier on the mind when branching all over the place, use one of these two: [me](https://techcommons.stanford.edu/topics/git/show-git-branch-bash-prompt) or [me](https://github.com/magicmonty/bash-git-prompt).
Commits should be formatted according to [this](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)

### Our rules
When working on a new feature/issue, you create a new branch for it. If possible, branch it from master so as to not inherit whatever bugs may be hidden in the other branches, however if you need unmerged work choose the branch accordingly. They may seem a bit strict/complicated, but I have discussed it with several EPFL IT proffessionals and they think it the minimum! ^-^ Also, getting used to do this here will be great when we will have to dev for bigger projects in real life.

We have three main branches, *master*, *prod*, and *staging*.
#### Dev
The dev branch is the default branch. If you follow correctly the workflow -- i.e. create a branch for a new issue/feature -- it shouldn't be used too much. You should instead use a branch you have created, and write your code/tests there.

#### Staging
Staging is the second state of your code: it is the branch where all the automatic tests will be setup (formatting, names, and compilation). As of 2016-02-21, none of these are setup, but they should come soon enough. All of this will be acheived through [gitlab-ci](https://about.gitlab.com/gitlab-ci/) and pre-commit hooks. Stay patient, you will end-up getting in. Once your code is accepted by the pre-commit hooks and builds satisfactorily - as well as passing any tests we/our teachers have provided, then you can create a merge request from staging to master.

#### Master
This branch enables us to use the best tool in the gitlab arsenal and the *ONE* thing to do when trying for quality: peer code review. When you open a merge request from a working staging commit to master, you should assign the other developper to the merge request. He will then review your code using the available git CLI/WebUI tools (WebUI is encouraged for the reason that you can open comments on certain parts of the code and discuss them there). He can suggest changes, that are your responsability to re-implement through the chain: dev on your laptop, then push to staging, ensure all is correct and then modify your merge request to point to the new commit. Once the other developper is satisfied with your code, he will do the merge.

This will allow us to easily output high-quality code, ensure that we both know what the other is doing (necessary for the exam / sharing ideas)
