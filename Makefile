.PHONY : help all clean list_tests doc

# set the classpath used for all compilations:
C=bin:.:/usr/share/java/hamcrest-core.jar:/usr/share/java/junit4.jar:lib/sq.jar
S=./src:./images

# Generic methods.
help:
	@echo "Available targets:"
	@echo "  all: compiles everything. (Add new sections by hand though)"
	@echo "  Class: compiles the said class."
	@echo "  ClassTest: compiles and runs the JUnit test called ClassTest."
	@echo "  namechecks: tries to compile all namechecks to make sure naming is correct."
	@echo "  doc: Build project documentation."
	@echo "  opendoc: Open documentation in firefox."
	@echo "  stats: create and open git statistics with gitstats."

all: ArgumentChecker Cell Direction Lists PlayerID RunLengthEncoder SubCell Time Block Board Bomb Bonus GameState GameStateSerializer Level Player Ticks GameStatePrinter RandomEventGenerator BlockImage BoardPainter ExplosionPainter PlayerPainter ClientGameState GameStateDeserializer ImageCollection KeyboardEventHandler XBlastComponent ClientMain ServerMain

tests: namechecks ListsTest BlockTest BombTest GameStateTest PlayerTest PaintersTest GameStateDeserializerTest

project: all tests doc stats

namechecks:
	@echo -n "Running $@... "
	@javac -cp $C -sourcepath $S -d ./bin test/ch/epfl/xblast/namecheck/*.java
	@echo "ok!"

## documentation
doc:
	@echo "Building documentation..."
	@javadoc -sourcepath $S -d doc -classpath $C -link http://cs108.epfl.ch/p/sq/ -link http://docs.oracle.com/javase/8/docs/api/ -subpackages ch.epfl

opendoc:
	@echo "Opening documentation files ..."
	@firefox doc/index.html

stats:
	@gitstats -c project_name=Xblast . out
	@firefox out/index.html

## files (Also looks for a JUnit file with the sameNameTest):
ArgumentChecker:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/$@.java
	@echo "ok!"

Cell:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/$@.java
	@echo "ok!"

Direction:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/$@.java
	@echo "ok!"

Lists:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/$@.java
	@echo "ok!"

PlayerID: ArgumentChecker
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/$@.java
	@echo "ok!"

RunLengthEncoder:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/$@.java
	@echo "ok!"

SubCell:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/$@.java
	@echo "ok!"

Time:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/$@.java
	@echo "ok!"

Block:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/$@.java
	@echo "ok!"

Board: Cell Lists
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/$@.java
	@echo "ok!"

Bomb: ArgumentChecker Cell Direction PlayerID
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/$@.java
	@echo "ok!"

Bonus:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/$@.java
	@echo "ok!"

GameState: ArgumentChecker Cell Direction Lists PlayerID SubCell Player
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/$@.java
	@echo "ok!"

GameStateSerializer: RunLengthEncoder GameState BoardPainter ExplosionPainter PlayerPainter
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/$@.java
	@echo "ok!"

Level: GameState BoardPainter
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/$@.java
	@echo "ok!"

Player: ArgumentChecker Cell Direction PlayerID SubCell
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/$@.java
	@echo "ok!"

Ticks: Time
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/$@.java
	@echo "ok!"

GameStatePrinter: Cell Block Board GameState Player
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/debug/$@.java
	@echo "ok!"

RandomEventGenerator: Direction PlayerID
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/debug/$@.java
	@echo "ok!"

BlockImage:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/painters/$@.java
	@echo "ok!"

BoardPainter: Cell Direction Block Board
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/painters/$@.java
	@echo "ok!"

ExplosionPainter: Bomb
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/painters/$@.java
	@echo "ok!"

PlayerPainter: Direction Player
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/painters/$@.java
	@echo "ok!"

GameStateDeserializer: 
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/client/$@.java
	@echo "ok!"

ClientGameState: SubCell PlayerID
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/client/GameState.java
	@echo "ok!"

ImageCollection:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/client/$@.java
	@echo "ok!"

KeyboardEventHandler:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/client/$@.java
	@echo "ok!"

XBlastComponent:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/client/$@.java
	@echo "ok!"

ClientMain:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/client/Main.java
	@echo "ok!"

ServerMain:
	@echo -n "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/Main.java
	@echo "ok!"

## tests - files
ListsTest: Lists
	@echo "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin test/ch/epfl/xblast/$@.java
	@echo "Running $@... "
	@java -cp $C  org.junit.runner.JUnitCore ch.epfl.xblast.$@

RunLengthEncoderTest: RunLengthEncoder
	@echo "Compiling $@... "
	@javac -Xlint -cp $C -sourcepath $S -d ./bin test/ch/epfl/xblast/$@.java
	@echo "Running $@... "
	@java -cp $C  org.junit.runner.JUnitCore ch.epfl.xblast.$@

BlockTest: Block
	@echo "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin test/ch/epfl/xblast/server/$@.java
	@echo "Running $@... "
	@java -cp $C  org.junit.runner.JUnitCore ch.epfl.xblast.server.$@

BombTest: Bomb
	@echo "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin test/ch/epfl/xblast/server/$@.java
	@echo "Running $@... "
	@java -cp $C  org.junit.runner.JUnitCore ch.epfl.xblast.server.$@

GameStateTest: GameState
	@echo "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin test/ch/epfl/xblast/server/$@.java
	@echo "Running $@... "
	@java -cp $C  org.junit.runner.JUnitCore ch.epfl.xblast.server.$@

PlayerTest: Player
	@echo "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin test/ch/epfl/xblast/server/$@.java
	@echo "Running $@... "
	@java -cp $C  org.junit.runner.JUnitCore ch.epfl.xblast.server.$@

PaintersTest: BoardPainter ExplosionPainter PlayerPainter
	@echo "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin test/ch/epfl/xblast/server/painters/$@.java
	@echo "Running $@... "
	@java -cp $C  org.junit.runner.JUnitCore ch.epfl.xblast.server.painters.$@

GameStateDeserializerTest: GameStateDeserializer ImageCollection
	@echo "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin test/ch/epfl/xblast/client/$@.java
	@echo "Running $@... "
	@java -cp $C ch.epfl.xblast.client.$@

GameStateGraphicPrint: all 
	@echo "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin test/ch/epfl/xblast/client/$@.java
	@echo "Running $@... "
	@java -cp $C ch.epfl.xblast.client.$@

runClient:
	@echo "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/client/Main.java
	@echo "Running $@... "
	@java -cp $C ch.epfl.xblast.client.Main

runServer:
	@echo "Compiling $@... "
	@javac -cp $C -sourcepath $S -d ./bin src/ch/epfl/xblast/server/Main.java
	@echo "Running $@... "
	@java -cp $C ch.epfl.xblast.server.Main 1
