package ch.epfl.xblast.client;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameStateDeserializerTest {
    public static void main(String[] args) {
        List<Byte> serial = Arrays.asList(
		new Byte[] {121, -50, 2, 1, -2, 0, 3, 1, 3, 1, -2, 0, 1, 1, 3, 1, 3,
		 1, 3, 1, 1, -2, 0, 1, 3, 1, 3, -2, 0, -1, 1, 3, 1, 3, 1,
		 3, 1, 1, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3,
		 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2,
		 3, 1, 0, 0, 3, 1, 3, 1, 0, 0, 1, 1, 3, 1, 1, 0, 0, 1, 3,
		 1, 3, 0, 0, -1, 1, 3, 1, 1, -5, 2, 3, 2, 3, -5, 2, 3, 2,
		 3, 1, -2, 0, 3, -2, 0, 1, 3, 2, 1, 2,
		 // bombs and such
		 4, -128, 16, -63, 16,
		 // players
		 3, 24, 24, 6,
		 3, -40, 24, 26,
		 3, -40, -72, 46,
		 3, 24, -72, 66,
		 // time
		 60});

        GameState output = GameStateDeserializer.deserializeGameState(serial);
    }
}
