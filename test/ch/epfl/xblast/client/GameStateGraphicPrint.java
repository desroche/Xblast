package ch.epfl.xblast.client;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.Map;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;

import ch.epfl.xblast.Cell;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.PlayerAction;
import ch.epfl.xblast.server.Block;
import ch.epfl.xblast.server.Board;
import ch.epfl.xblast.server.GameState;
import ch.epfl.xblast.server.GameStateSerializer;
import ch.epfl.xblast.server.Player;
import ch.epfl.xblast.server.debug.RandomEventGenerator;
import ch.epfl.xblast.server.painters.BoardPainter;

public class GameStateGraphicPrint {

    public static void main(String[] args) {
        JFrame j = new JFrame("Test");
        j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        RandomEventGenerator rdm = new RandomEventGenerator(2016, 30, 100);
        BoardPainter b = BoardPainter.DEFAULT_PAINTER;
        Map<Integer, PlayerAction> kb = new HashMap<>();
        
        // actions
        kb.put(KeyEvent.VK_UP, PlayerAction.MOVE_N);
        kb.put(KeyEvent.VK_RIGHT, PlayerAction.MOVE_E);
        kb.put(KeyEvent.VK_DOWN, PlayerAction.MOVE_S);
        kb.put(KeyEvent.VK_LEFT, PlayerAction.MOVE_W);
        kb.put(KeyEvent.VK_SPACE, PlayerAction.DROP_BOMB);
        kb.put(KeyEvent.VK_SHIFT, PlayerAction.STOP);

	// Blocks
        Block __ = Block.FREE;
        Block XX = Block.INDESTRUCTIBLE_WALL;
        Block xx = Block.DESTRUCTIBLE_WALL;
        Board board = Board.ofQuadrantNWBlocksWalled(
          Arrays.asList(
            Arrays.asList(__, __, __, __, __, xx, __),
            Arrays.asList(__, XX, xx, XX, xx, XX, xx),
            Arrays.asList(__, xx, __, __, __, xx, __),
            Arrays.asList(xx, XX, __, XX, XX, XX, XX),
            Arrays.asList(__, xx, __, xx, __, __, __),
            Arrays.asList(xx, XX, xx, XX, xx, XX, __)));
	
	// Players
        List<Player> players = new ArrayList<>();
        players.add(new Player(PlayerID.PLAYER_1, 3, new Cell(1, 1), 2, 3));
        players.add(new Player(PlayerID.PLAYER_2, 3, new Cell(13, 1), 2, 3));
        players.add(new Player(PlayerID.PLAYER_3, 3, new Cell(13, 11), 2, 3));
        players.add(new Player(PlayerID.PLAYER_4, 3, new Cell(1, 11), 2, 3));
        
        
        GameState actual = new GameState(board, players);
        XBlastComponent grapInte = new XBlastComponent();
        List<Byte> ser = GameStateSerializer.serialize(actual, b);
        ch.epfl.xblast.client.GameState s = GameStateDeserializer.deserializeGameState(ser);
        grapInte.setGameState(s, PlayerID.PLAYER_1);

        Consumer<PlayerAction> c = System.out::println;
        grapInte.addKeyListener(new KeyboardEventHandler(kb, c));
        j.getContentPane().add(grapInte);
        j.pack();
        j.setVisible(true);
        grapInte.requestFocusInWindow();
        
        while(!actual.isGameOver()) {
            actual = actual.next(rdm.randomSpeedChangeEvents(), rdm.randomBombDropEvents());
            ser = GameStateSerializer.serialize(actual, b);
            s = GameStateDeserializer.deserializeGameState(ser);
            grapInte.setGameState(s, PlayerID.PLAYER_1);
        }
    }

}
