package ch.epfl.xblast;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class ListsTest {

    @Test
    public void symmetricWithANormalList() {
        List<String> test1 = new ArrayList<String>();
        test1.add("Bonjour");
        test1.add("au");
        test1.add("suisses");

        List<String> exc = new ArrayList<String>();
        exc.addAll(test1);
        exc.add("au");
        exc.add("Bonjour");

        List<String> get = Lists.mirrored(test1);

        for (int i = 0; i < get.size(); ++i) {
            assertEquals(exc.get(i), get.get(i));
        }
    }

    @Test
    public void symmetricWithListOneMember() {
        List<String> test1 = new ArrayList<String>();
        test1.add("Yo");
        assertEquals(Lists.mirrored(test1).get(0), "Yo");
    }

    @Test(expected = IllegalArgumentException.class)
    public void symmetricEmpty() {
        List<String> test1 = new ArrayList<String>();
        Lists.mirrored(test1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void symmetricNull() {
        List<Integer> test1 = null;
        Lists.mirrored(test1);
    }
}
