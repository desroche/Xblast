package ch.epfl.xblast;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class RunLengthEncoderTest {

    @Test
    public void RunLengthBijective() {
	    assertEquals(list1(), RunLengthEncoder.decoder(RunLengthEncoder.encoder(list1())));
	    assertEquals(list2(), RunLengthEncoder.decoder(RunLengthEncoder.encoder(list2())));
	    assertEquals(list3(), RunLengthEncoder.decoder(RunLengthEncoder.encoder(list3())));
    }

    @Test
    public void RunLengthEncode() {
	List<Byte> et1 = new ArrayList<>();
	List<Byte> et2 = new ArrayList<>();
	List<Byte> et3 = new ArrayList<>();

	et1.add((byte)1);
	et1.add((byte)2);
	et1.add((byte)2);
	et1.add((byte)-1);
	et1.add((byte)3);
	et1.add((byte)-2);
	et1.add((byte)4);

        et2.add((byte)-2);
        et2.add((byte)4);
	et2.add((byte)-1);
	et2.add((byte)3);
	et2.add((byte)2);
	et2.add((byte)2);
	et2.add((byte)1);

	et3.add((byte)2);
	et3.add((byte)2);
	et3.add((byte)-1);
	et3.add((byte)3);
        et3.add((byte)-2);
        et3.add((byte)4);
	et3.add((byte)1);

        assertEquals(et1, RunLengthEncoder.encoder(list1()));
        assertEquals(et2, RunLengthEncoder.encoder(list2()));
        assertEquals(et3, RunLengthEncoder.encoder(list3()));
    }

    @Test
    public void RunLengthOverflow() {
	    List<Byte> shouldbe = new ArrayList<>();
	    List<Byte> input = new ArrayList<>();
	    
	    for (int i = 0; i < 150; i++) {
		input.add((byte) 5 );
	    }

	    shouldbe.add((byte) -128);
	    shouldbe.add((byte) 5);
	    shouldbe.add((byte) -18);
	    shouldbe.add((byte) 5);
    }
    private List<Byte> list1() {
        List<Byte> clearText = new ArrayList<>();

        clearText.add((byte)0b00000001);

        clearText.add((byte)0b00000010);
        clearText.add((byte)0b00000010);

        clearText.add((byte)0b00000011);
        clearText.add((byte)0b00000011);
        clearText.add((byte)0b00000011);

        clearText.add((byte)0b00000100);
        clearText.add((byte)0b00000100);
        clearText.add((byte)0b00000100);
        clearText.add((byte)0b00000100);

        return clearText; 
    }

    private List<Byte> list2() {
        List<Byte> clearText = new ArrayList<>();

        clearText.add((byte)0b00000100);
        clearText.add((byte)0b00000100);
        clearText.add((byte)0b00000100);
        clearText.add((byte)0b00000100);

        clearText.add((byte)0b00000011);
        clearText.add((byte)0b00000011);
        clearText.add((byte)0b00000011);

        clearText.add((byte)0b00000010);
        clearText.add((byte)0b00000010);

        clearText.add((byte)0b00000001);

        return clearText; 
    }

    private List<Byte> list3() {
	    List<Byte> clearText = new ArrayList<>();

	    clearText.add((byte)0b00000010);
	    clearText.add((byte)0b00000010);

	    clearText.add((byte)0b00000011);
	    clearText.add((byte)0b00000011);
	    clearText.add((byte)0b00000011);

	    clearText.add((byte)0b00000100);
	    clearText.add((byte)0b00000100);
	    clearText.add((byte)0b00000100);
	    clearText.add((byte)0b00000100);

	    clearText.add((byte)0b00000001);

	    return clearText; 
    }
}
