package ch.epfl.xblast.server.painters;

import org.junit.Test;

import ch.epfl.cs108.Sq;

import ch.epfl.xblast.Cell;
import ch.epfl.xblast.SubCell;
import ch.epfl.xblast.Direction;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.server.*;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PaintersTest {
    
    @Test
    public void ordinalBlockImages(){
        assertEquals(0, BlockImage.IRON_FLOOR.ordinal());
        assertEquals(1, BlockImage.IRON_FLOOR_S.ordinal());
        assertEquals(2, BlockImage.DARK_BLOCK.ordinal());
        assertEquals(3, BlockImage.EXTRA.ordinal());
        assertEquals(4, BlockImage.EXTRA_O.ordinal());
        assertEquals(5, BlockImage.BONUS_BOMB.ordinal());
        assertEquals(6, BlockImage.BONUS_RANGE.ordinal());
    }
    
    @Test(expected = NullPointerException.class)
    public void boardPainterNullTest1(){
        Map<Block, BlockImage> painter = null;
        BoardPainter b = new BoardPainter(painter, BlockImage.DARK_BLOCK);
    }

    @Test(expected = NullPointerException.class)
    public void boardPainterNullTest2(){
        Map<Block, BlockImage> painter = null;
        BoardPainter b = new BoardPainter(painter, null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void boardPainterWrongNumber(){
        Map<Block, BlockImage> painter = new HashMap<>();
        painter.put(Block.FREE, BlockImage.IRON_FLOOR);
        painter.put(Block.BONUS_BOMB, BlockImage.BONUS_BOMB);
        BoardPainter b = new BoardPainter(painter, BlockImage.EXTRA_O);
    }
    
    @Test
    public void boardPainterTestWithRealBoard(){
        Block __ = Block.FREE;
        Block XX = Block.INDESTRUCTIBLE_WALL;
        Block xx = Block.DESTRUCTIBLE_WALL;
        Board board = Board.ofQuadrantNWBlocksWalled(
          Arrays.asList(
            Arrays.asList(__, __, __, __, __, xx, __),
            Arrays.asList(__, XX, xx, XX, xx, XX, xx),
            Arrays.asList(__, xx, __, __, __, xx, __),
            Arrays.asList(xx, XX, __, XX, XX, XX, XX),
            Arrays.asList(__, xx, __, xx, __, __, __),
            Arrays.asList(xx, XX, xx, XX, xx, XX, __)));
        
        Map<Block, BlockImage> painter = new HashMap<>();
        painter.put(Block.FREE, BlockImage.IRON_FLOOR);
        painter.put(Block.INDESTRUCTIBLE_WALL, BlockImage.DARK_BLOCK);
        painter.put(Block.BONUS_RANGE, BlockImage.BONUS_RANGE);
        painter.put(Block.BONUS_BOMB, BlockImage.BONUS_BOMB);
        painter.put(Block.DESTRUCTIBLE_WALL, BlockImage.EXTRA);
        painter.put(Block.CRUMBLING_WALL, BlockImage.EXTRA_O);
        BoardPainter b = new BoardPainter(painter, BlockImage.IRON_FLOOR_S);
        
        assertEquals(b.byteForCell(board, new Cell(0,0)) , BlockImage.DARK_BLOCK.ordinal());
        assertEquals(b.byteForCell(board, new Cell(1,1)) , BlockImage.IRON_FLOOR_S.ordinal());
        assertEquals(b.byteForCell(board, new Cell(2,3)) , BlockImage.EXTRA.ordinal());
        assertEquals(b.byteForCell(board, new Cell(7,1)) , BlockImage.IRON_FLOOR_S.ordinal());
        assertEquals(b.byteForCell(board, new Cell(2,1)) , BlockImage.IRON_FLOOR.ordinal());
    }
    
    @Test
    public void PlayerPainterTestAsInProblem(){
        // Create a vulnerable player with 3 lives, facing east and with (27,24) as a position. 
        Player guinneaPig = new Player(PlayerID.PLAYER_1,
                Sq.constant(new Player.LifeState(3, Player.LifeState.State.VULNERABLE)),
                Sq.constant(new Player.DirectedPosition(new SubCell(27,24), Direction.E)),
                    5,5);

        // Paint that player.
        assertEquals(5, PlayerPainter.byteForPlayer(5,guinneaPig));
    }

    @Test
    public void PlayerPainterTestIsWASP_NO_IM_NOT_RACIST() {
        // Create an invulnerable player with 3 lives, facing east and with (27,24) as a position. 
        Player guinneaPig = new Player(PlayerID.PLAYER_1,
                Sq.constant(new Player.LifeState(3, Player.LifeState.State.INVULNERABLE)),
                Sq.constant(new Player.DirectedPosition(new SubCell(27,24), Direction.E)),
                    5,5);

        // Paint that player, he should be WHITE as the number of ticks is uneven.
        assertEquals(85, PlayerPainter.byteForPlayer(5 ,guinneaPig));
    }

    @Test
    public void PlayerPainterTestPlayerThreeDyingNoLivesLeft(){
        // Create an invulnerable player with 3 lives, facing east and with (27,24) as a position. 
        Player guinneaPig = new Player(PlayerID.PLAYER_3,
                Sq.constant(new Player.LifeState(1, Player.LifeState.State.DYING)),
                Sq.constant(new Player.DirectedPosition(new SubCell(27,24), Direction.S)),
                    5,5);

        // Paint that player, he should be WHITE as the number of ticks is uneven.
        assertEquals(53, PlayerPainter.byteForPlayer(5 ,guinneaPig));
    }
    
    @Test
    public void blastPrintTest(){
        assertEquals(ExplosionPainter.byteForBlast(false, false, false, false), 0b0000);
        assertEquals(ExplosionPainter.byteForBlast(true, false, false, false), 0b0001);
        assertEquals(ExplosionPainter.byteForBlast(false, true, false, false), 0b0010);
        assertEquals(ExplosionPainter.byteForBlast(false, false, true, false), 0b0100);
        assertEquals(ExplosionPainter.byteForBlast(false, false, false, true), 0b1000);
        assertEquals(ExplosionPainter.byteForBlast(true, true, true, true), 0b1111);
        assertEquals(ExplosionPainter.byteForBlast(true, false, false, true), 0b1001);
    }
    
}
