package ch.epfl.xblast.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.HashSet;

import org.junit.Test;

import ch.epfl.cs108.Sq;
import ch.epfl.xblast.Cell;
import ch.epfl.xblast.PlayerID;

public class GameStateTest {
	@Test
	public void emptyForMakefile() {
		assertTrue(true);
	}
//    @Test
//    public void NewBombTest() {
//        List<Player> Players = createFourPlayers();
//        Set<PlayerID> potBombers = new HashSet<>();
//        potBombers.add(PlayerID.PLAYER_1);
//        List<Bomb> presentBombs = new ArrayList<>();
//        assertFalse(GameState.newlyDroppedBombs(Players, potBombers, presentBombs).isEmpty());
//    }
//
//    @Test
//    public void newBombDead() {
//        List<Player> Players = createFourPlayersOneAlive();
//        Set<PlayerID> potBombers = new HashSet<>();
//        potBombers.add(PlayerID.PLAYER_2);
//        potBombers.add(PlayerID.PLAYER_3);
//        potBombers.add(PlayerID.PLAYER_4);
//        List<Bomb> presentBombs = new ArrayList<>();
//        assertTrue(GameState.newlyDroppedBombs(Players, potBombers, presentBombs).isEmpty());
//    }
//
//    @Test
//    public void ConflictBombTest() {
//        List<Player> Players = createFourPlayers();
//        Set<PlayerID> potBombers = new HashSet<>();
//        potBombers.add(PlayerID.PLAYER_2);
//	  potBombers.add(PlayerID.PLAYER_3);
//        potBombers.add(PlayerID.PLAYER_1);
//        List<Bomb> presentBombs = new ArrayList<>();
//	presentBombs.add(new Bomb(PlayerID.PLAYER_4, new Cell(1,11), 5, 5));
//	System.out.println(GameState.newlyDroppedBombs(Players, potBombers, presentBombs));
//        assertTrue(GameState.newlyDroppedBombs(Players, potBombers, presentBombs).size() == 1);
//        assertTrue(GameState.newlyDroppedBombs(Players, potBombers, presentBombs).get(0).ownerId() == PlayerID.PLAYER_2);
//    }
//	@Test
//	public void printElemState() {
//		System.out.println();
//		ch.epfl.xblast.server.debug.GameStatePrinter.printGameState(new GameState(createFreeWalledBoard(), createFourPlayers()));
//	}
//
//	@Test(expected = NullPointerException.class)
//	public void nullCheck() {
//		GameState gg = new GameState(null, null);
//	}
//
//	@Test(expected = IllegalArgumentException.class)
//	public void illegCheck() {
//		GameState g = new GameState(-3, null, null, null, null, null);
//	}
//
//	@Test
//	public void gameOverTime() {
//		int max = Ticks.TOTAL_TICKS;
//		GameState g = new GameState(max+1, createFreeWalledBoard(), createFourPlayers(), new ArrayList<Bomb>(), new ArrayList<Sq<Sq<Cell>>>(), new ArrayList<Sq<Cell>>());
//		assertTrue(g.isGameOver());
//		assertTrue(!g.winner().isPresent());
//	}
//
//	@Test
//	public void winning() {
//		int max = Ticks.TOTAL_TICKS;
//		GameState g = new GameState(createFreeWalledBoard(), createFourPlayersOneAlive());
//		assertTrue(g.isGameOver());
//		assertEquals(Optional.of(PlayerID.PLAYER_1), g.winner());
//		assertTrue(g.winner().isPresent());
//	}
//
//	@Test
//	public void emptyOptional() {
//		GameState g = new GameState(createFreeWalledBoard(), createFourPlayers());
//		assertTrue(!g.winner().isPresent());
//	}
//
//	@Test
//	public void naturalBlastDie() {
//		Cell pos = new Cell(5,5);
//		Sq<Cell> blast0 = Sq.constant(pos).limit(1);
//		List<Sq<Cell>> blasts0 = new ArrayList<>();
//		blasts0.add(blast0);
//		assertTrue(GameState.nextBlasts(blasts0, createFreeWalledBoard(), new ArrayList<Sq<Sq<Cell>>>()).isEmpty());
//	}
//
//	@Test
//	public void nonFreeBlastDie() {
//		Cell pos = new Cell(0,0);
//		Sq<Cell> blast0 = Sq.constant(pos).limit(1);
//		List<Sq<Cell>> blasts0 = new ArrayList<>();
//		blasts0.add(blast0);
//		assertTrue(GameState.nextBlasts(blasts0, createFreeWalledBoard(), new ArrayList<Sq<Sq<Cell>>>()).isEmpty());
//	}
//
//	@Test
//	public void blastCreationFromExplosion() {
//		Cell pos = new Cell(5,5);
//		Sq<Cell> blast0 = Sq.constant(pos).limit(5);
//		Sq<Sq<Cell>> explosion0 = Sq.constant(blast0).limit(5);
//		List<Sq<Cell>> blasts0 = new ArrayList<>();
//		List<Sq<Sq<Cell>>> explosions0 = new ArrayList<>();
//		explosions0.add(explosion0);
//		assertTrue(GameState.nextBlasts(blasts0, createFreeWalledBoard(), explosions0).get(0).head().equals(pos));
//	}
//
	private Board createFreeWalledBoard() {
		List<List<Block>> quadrant = new ArrayList<List<Block>>();
		List<Block> quadrow = new ArrayList<Block>();
		for (int i = 0; i < 7; i++) {
			quadrow.add(Block.FREE);
		}
		for (int i = 0; i < 6; i++) {
			quadrant.add(quadrow);
		}
		return Board.ofQuadrantNWBlocksWalled(quadrant);
	}

	private List<Player> createFourPlayers() {
		List<Player> players = new ArrayList<>();
		Player p1 = new Player(PlayerID.PLAYER_1, 3, new Cell(1, 1), 3, 1 );
		Player p2 = new Player(PlayerID.PLAYER_2, 3, new Cell(1, 1), 3, 1 );
		Player p3 = new Player(PlayerID.PLAYER_3, 1, new Cell(1, 11), 3, 1 );
		Player p4 = new Player(PlayerID.PLAYER_4, 1, new Cell(13, 11), 3, 1 );
		players.add(p4);
		players.add(p2);
		players.add(p3);
		players.add(p1);
		return players;
	}

	private List<Player> createFourPlayersOneAlive() {
		List<Player> players = new ArrayList<>();
		Player p1 = new Player(PlayerID.PLAYER_1, 3, new Cell(1, 1), 3, 1 );
		Player p2 = new Player(PlayerID.PLAYER_2, 0, new Cell(13, 1), 3, 1 );
		Player p3 = new Player(PlayerID.PLAYER_3, 0, new Cell(1, 11), 3, 1 );
		Player p4 = new Player(PlayerID.PLAYER_4, 0, new Cell(13, 11), 3, 1 );
		players.add(p2);
		players.add(p1);
		players.add(p4);
		players.add(p3);
		return players;
	}
}
