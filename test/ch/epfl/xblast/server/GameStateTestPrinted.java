package ch.epfl.xblast.server;

import ch.epfl.xblast.ArgumentChecker;
import ch.epfl.xblast.Cell;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.server.debug.GameStatePrinter;
import ch.epfl.xblast.server.debug.RandomEventGenerator;
import java.util.*;



public class GameStateTestPrinted {
    
    private static Scanner keyg = new Scanner(System.in);
    public static final int MAX_TICKS = 1500;
    
    public static void main(String[] args) {
       RandomEventGenerator rdm = new RandomEventGenerator(2016, 30, 100);
       
       Block __ = Block.FREE;
       Block XX = Block.INDESTRUCTIBLE_WALL;
       Block xx = Block.DESTRUCTIBLE_WALL;
       Board board = Board.ofQuadrantNWBlocksWalled(
         Arrays.asList(
           Arrays.asList(__, __, __, __, __, xx, __),
           Arrays.asList(__, XX, xx, XX, xx, XX, xx),
           Arrays.asList(__, xx, __, __, __, xx, __),
           Arrays.asList(xx, XX, __, XX, XX, XX, XX),
           Arrays.asList(__, xx, __, xx, __, __, __),
           Arrays.asList(xx, XX, xx, XX, xx, XX, __)));
       List<Player> players = new ArrayList<>();
       players.add(new Player(PlayerID.PLAYER_1, 3, new Cell(1, 1), 2, 3));
       players.add(new Player(PlayerID.PLAYER_2, 3, new Cell(13, 1), 2, 3));
       players.add(new Player(PlayerID.PLAYER_3, 3, new Cell(13, 11), 2, 3));
       players.add(new Player(PlayerID.PLAYER_4, 3, new Cell(1, 11), 2, 3));
       
       
       int totTicks = 0;
       GameState actual = new GameState(board, players);
       
       while(!actual.isGameOver()) {
           System.out.println("How many net ticks : ");
           int numberOfTicks = ArgumentChecker.requireNonNegative(keyg.nextInt());
           totTicks += numberOfTicks;
           actual = goTo(numberOfTicks, actual, rdm);
           GameStatePrinter.printGameState(actual);
           System.out.println();
       }
       
       
    }
    
    public static GameState goTo(int ticks, GameState s, RandomEventGenerator rdm){
        for(int i = 0; i < ticks; ++i){
            s = s.next(rdm.randomSpeedChangeEvents(), rdm.randomBombDropEvents());
        }
        return s;
    }

}
