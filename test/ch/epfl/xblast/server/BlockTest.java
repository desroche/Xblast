package ch.epfl.xblast.server;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BlockTest {

    @Test
    public void blockTestResult() {
        assertEquals(Block.INDESTRUCTIBLE_WALL.isFree(), false);
        assertEquals(Block.FREE.castsShadow(), false);
    }

}
