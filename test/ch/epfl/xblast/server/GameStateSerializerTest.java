package ch.epfl.xblast.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ch.epfl.xblast.Cell;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.server.painters.BoardPainter;

public class GameStateSerializerTest {

    public static void main(String[] args) {
        
        
        List<Player> players = new ArrayList<>();
        players.add(new Player(PlayerID.PLAYER_1, 3, new Cell(1, 1), 2, 3));
        players.add(new Player(PlayerID.PLAYER_2, 3, new Cell(13, 1), 2, 3));
        players.add(new Player(PlayerID.PLAYER_3, 3, new Cell(13, 11), 2, 3));
        players.add(new Player(PlayerID.PLAYER_4, 3, new Cell(1, 11), 2, 3));
        
        GameState actual = new GameState(Board.DEFAULT_BOARD, players);
        BoardPainter b = BoardPainter.DEFAULT_PAINTER;
        
        System.out.println(GameStateSerializer.serialize(actual, b));

    }

}
