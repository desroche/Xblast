package ch.epfl.xblast;

/**
 * The PlayerID enum creates different attributes to distinguish the
 * players.
 *
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public enum PlayerID {
    PLAYER_1, PLAYER_2, PLAYER_3, PLAYER_4;
}
