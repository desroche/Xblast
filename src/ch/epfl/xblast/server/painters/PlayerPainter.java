package ch.epfl.xblast.server.painters;

import ch.epfl.xblast.Direction;
import ch.epfl.xblast.server.Player;

/**
 * Uninstanciable painter for the Players.
 *
 * Allows getting the images for players depending on the state of the game.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class PlayerPainter {
    // As PlayerPainter contains only static methods, we don't want anyone to instanciate it.
    private PlayerPainter() {}

    /**
     * Paints a Player according to his and the board's attributes.
     *
     * @param tick
     *     The tick at which we want to paint the player.
     * @param subject
     *     The Player that will be painted by this  method.
     * @return
     *     A byte representing the number of the image used to represent the player on the board.
     */
    public static byte byteForPlayer(int tick, Player subject) throws IllegalArgumentException {
        /*
         * Any player's image for any state can be obtained by summing a number corresponding to the player and to the state
         * we want to paint, as they are always ordered in the same way for every player, and there always are the same number of 
         * images per player.
         */

        // Store the constants that we are going to switch on.
        Player.LifeState.State subjectState = subject.lifeState().state();

        // Find out the player's magic number.
        int whatPlayer = subject.id().ordinal() * 20;

        // Deal with each state separately.
	int whatState;
        switch(subjectState) { 
            case DEAD: 
                // Send an invalid image code
                whatState = 15;
                break;

            case DYING:
                whatState = subject.lives() > 1 ? 12 : 13;
                break;

            case INVULNERABLE:
                if (tick % 2 == 1) {whatPlayer = 80;}

            case VULNERABLE:
                // Store the direction as we are going to use it several times.
                Direction subjectDir = subject.direction();

                // Find the magic number for the direction the player is facing.
                int whatDirection = subjectDir.ordinal() * 3;

                // Find which of the three images are to be used (simulate walking).
                int whatPosition = (subjectDir.isHorizontal() ? subject.position().x() : subject.position().y()) % 4;
		int whatWalk;
                if (whatPosition % 2 == 0) {
                    whatWalk = 0;
                } else if (whatPosition == 1) {
                    whatWalk = 1;
                } else if (whatPosition == 3) {
                    whatWalk = 2;
                } else {
                    throw new IllegalStateException("FUBAR");
                }
                whatState = whatWalk + whatDirection;  
		break;

            default:
                throw new IllegalStateException("SNAFU");
            }

        // All done o/
        return (byte) (whatState + whatPlayer); 
    }
}
