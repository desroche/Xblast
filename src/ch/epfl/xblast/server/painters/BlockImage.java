package ch.epfl.xblast.server.painters;

/**
 * The different images representing blocks on the board.
 *
 * @see ch.epfl.xblast.server.Block
 * @see ch.epfl.xblast.server.Board
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public enum BlockImage {
    IRON_FLOOR, IRON_FLOOR_S, DARK_BLOCK, EXTRA, EXTRA_O, BONUS_BOMB, BONUS_RANGE;
}
