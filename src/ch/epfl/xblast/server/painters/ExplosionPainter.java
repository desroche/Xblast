package ch.epfl.xblast.server.painters;

import ch.epfl.xblast.server.Bomb;

/**
 * The class used to paint bomb and blast.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class ExplosionPainter {
    /*
     * Invalid image number to return when there is no explosion.
     */
    public final static byte BYTE_FOR_EMPTY = 16;
    
    /*
     * Private builder, we don't need to build an ExplosionPainter since there are only static methods.
     */
    private ExplosionPainter(){}
    
    /**
     * Return the byte corresponding to a Bomb set on the Board.
     * @param b The bomb we want to paint.
     * @return The byte of the image corresponding to the Bomb (black or white depending on his fuselength).
     */
    public static byte byteForBomb(Bomb b){
        return Integer.bitCount(b.fuseLength()) == 1 ? (byte) 21 : (byte) 20;
    }
    
    /**
     * Return the byte of the image corresponding to a blast.
     * @param isN If the Blast goes to the north.
     * @param isS If the Blast goes to the south.
     * @param isW If the Blast goes to the west.
     * @param isE If the Blast goes to the east.
     * @return the byte of the image, for the blast.
     */
    public static byte byteForBlast(boolean isW, boolean isS, boolean isE, boolean isN){
        byte toRet = 0b0000;
        if(isW)
            toRet = (byte)(toRet | 0b0001);
        if(isS)
            toRet = (byte)(toRet | 0b0010);
        if(isE)
            toRet = (byte)(toRet | 0b0100);
        if(isN)
            toRet = (byte)(toRet | 0b1000);
        return toRet != 0 ? toRet : BYTE_FOR_EMPTY;
    }
}
