package ch.epfl.xblast.server.painters;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import ch.epfl.xblast.Cell;
import ch.epfl.xblast.Direction;
import ch.epfl.xblast.server.Block;
import ch.epfl.xblast.server.Board;

/**
 * The class used to draw the Blocks of the Board.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class BoardPainter {
    // Class constants
    private final static int numOfBlocks = Block.values().length;
    /*
     * Associate each block to it's image.
     */
    private final Map<Block, BlockImage> palette;
    
    /*
     * The blocks that have a shadowed version.
     */
    private final BlockImage shadowedBlock;

    /**
     * The BoardPainter allowing us to create the 'default' level as per the
     * project specifications.
     */
    public static final BoardPainter DEFAULT_PAINTER = new BoardPainter(defaultPalette(), BlockImage.IRON_FLOOR_S);
    
    /**
     * Construct a new BoardPainter.
     * @param palette	The Map used to link each Block to a BlockImage
     * @param shadowedBlock The BlockImage used to represent a shadowed block.
     * @throws IllegalArgumentException If the palette does not provide an image for every possible block.	
     */
    public BoardPainter(Map<Block, BlockImage> palette, BlockImage shadowedBlock) throws IllegalArgumentException {
        if (Objects.requireNonNull(palette).size() != numOfBlocks) {
        	throw new IllegalArgumentException("We need a palette of a size of " + numOfBlocks + ", each block must be linked to a BlockImage)");
        } else {
        	this.palette = Collections.unmodifiableMap(new HashMap<>(palette));
        }
        this.shadowedBlock = Objects.requireNonNull(shadowedBlock);
    }
    
    /**
     * Return the byte corresponding to the Block to paint.
     * @param b The Board from where the block come from.
     * @param c The position of the Block we want to paint.
     * @return The BlockImage corresponding to the block at position c.
     */
    public byte byteForCell(Board b, Cell c) throws IllegalArgumentException{
        Block blockAtCell = b.blockAt(c);
        
        switch(blockAtCell){
        case FREE:
            if(b.blockAt(c.neighbor(Direction.W)).castsShadow()){
                return (byte) shadowedBlock.ordinal();
            }else{
                return (byte) palette.get(blockAtCell).ordinal();
            }
        case INDESTRUCTIBLE_WALL:
            return (byte) palette.get(blockAtCell).ordinal();
        case DESTRUCTIBLE_WALL:
            return (byte) palette.get(blockAtCell).ordinal();
        case CRUMBLING_WALL:
            return (byte) palette.get(blockAtCell).ordinal();
        case BONUS_BOMB:
            return (byte) palette.get(blockAtCell).ordinal();
        case BONUS_RANGE:
            return (byte) palette.get(blockAtCell).ordinal();
        default:
            // This shouldn't happen. If it does, check you've done your monthly sacrifice.
            throw new IllegalArgumentException("Unknown Block to draw.");
        }
    }

    /*
     * Helps building the default palette.
     */
    private final static Map<Block, BlockImage> defaultPalette() {
        Map<Block, BlockImage> defPalette = new HashMap<>();
        defPalette.put(Block.FREE, BlockImage.IRON_FLOOR);
        defPalette.put(Block.INDESTRUCTIBLE_WALL, BlockImage.DARK_BLOCK);
        defPalette.put(Block.DESTRUCTIBLE_WALL, BlockImage.EXTRA);
        defPalette.put(Block.CRUMBLING_WALL, BlockImage.EXTRA_O);
        defPalette.put(Block.BONUS_BOMB, BlockImage.BONUS_BOMB);
        defPalette.put(Block.BONUS_RANGE, BlockImage.BONUS_RANGE);
        return defPalette;
    }
}
