package ch.epfl.xblast.server;

import java.util.NoSuchElementException;

/**
 * Describes the different types of blocks we can encounter in the game.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 * @see Board
 */

public enum Block {
    FREE, INDESTRUCTIBLE_WALL, DESTRUCTIBLE_WALL, CRUMBLING_WALL, BONUS_BOMB(Bonus.INC_BOMB), BONUS_RANGE(Bonus.INC_RANGE);
    
    private final Bonus maybeAssociatedBonus;
    
    /**
     * Private builder for a Block without a Bonus
     */
    private Block(){
        this.maybeAssociatedBonus = null;
    }
    
    /**
     * Private builder for a Block with a Bonus
     * @param maybeAssociatedBonus  The associated bonus to the Block.
     */
    private Block(Bonus maybeAssociatedBonus){
        this.maybeAssociatedBonus = maybeAssociatedBonus;
    }
    
    /**
     * Return true if it's a free block, false otherwise.
     * 
     * @return true if the block is FREE, false otherwise.
     */
    public boolean isFree() {
        return (this == FREE);
    }

    /**
     * Return true if a player can stand on the block.
     * 
     * @return if a player can walk on the block (Free and Bonus) or not;
     */
    public boolean canHostPlayer() {
        return this.isFree() || this.isBonus();
    }

    /**
     * Return true if the block should cast a shadow on the board.
     * 
     * @return if the block is a WALL and should cast a shadow.
     */
    public boolean castsShadow() {
        return (this == INDESTRUCTIBLE_WALL 
                || this == DESTRUCTIBLE_WALL
                || this == CRUMBLING_WALL);
    }
    
    /**
     * Return true if the block is a Bonus.
     * @return if the Block is a bonus or not.
     */
    public boolean isBonus(){
        return (this == BONUS_BOMB || this == BONUS_RANGE);
    }
    
    /**
     * Return the associated Bonus of the Block.
     * @return the associated Bonus of the Block.
     * @throws NoSuchElementException if the Block doesn't have an associated Bonus.
     */
    public Bonus associatedBonus() throws NoSuchElementException{
        if (maybeAssociatedBonus == null) {
            throw new NoSuchElementException("No Bonus on this type of Block");
        } else {
            return maybeAssociatedBonus;
        }
    }
}
