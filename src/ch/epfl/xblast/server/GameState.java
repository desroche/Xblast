package ch.epfl.xblast.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.Collections;

import ch.epfl.cs108.Sq;
import ch.epfl.xblast.ArgumentChecker;
import ch.epfl.xblast.Cell;
import ch.epfl.xblast.Direction;
import ch.epfl.xblast.Lists;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.SubCell;
import ch.epfl.xblast.server.Player.DirectedPosition;
import ch.epfl.xblast.server.Player.LifeState;

/**
 * This class holds the state of the game at an instant t and is used mainly to
 * calculate the state of the game at the instant t+1.
 *
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class GameState {
    private final int ticks;
    private final Board board;
    private final List<Player> players;
    private final List<Bomb> bombs;
    private final List<Sq<Sq<Cell>>> explosions;
    private final List<Sq<Cell>> blasts;

    // List of possible permutations of the list of players.
    private static final List<List<PlayerID>> permutations = Lists
            .permutations(Arrays.asList(PlayerID.values()));

    // (not really) random event generator.
    private static final Random NOTSORANDOM = new Random(2016);

    /**
     * Default GameState at the beggining of a game.
     */
    public static final GameState DEFAULT_GAMESTATE = new GameState(Board.DEFAULT_BOARD, Player.DEFAULT_PLAYERS);

    /**
     * @param ticks
     *            The number of already elapsed ticks when we create the game.
     * @param board
     *            The board on which the game is played.
     * @param players
     *            A list containing the four players of the game.
     * @param bombs
     *            A list containing the bombs already on the board at build
     *            time.
     * @param explosions
     *            A list containing the explosions currently happening on the
     *            board at build time.
     * @param blasts
     *
     *            A list containing the explosion particles (blasts) that have
     *            been emitted prior to build time and are on the board.
     */
    public GameState(int ticks, Board board, List<Player> players,
            List<Bomb> bombs, List<Sq<Sq<Cell>>> explosions,
            List<Sq<Cell>> blasts) {
        this.ticks = ArgumentChecker
                .requireNonNegative(ticks);
        this.board = Objects.requireNonNull(board);
        if (Objects.requireNonNull(players).size() == PlayerID.values().length) {
            this.players = new ArrayList<>(Collections.unmodifiableList(players));
        } else {
            throw new IllegalArgumentException("There must be four players!");
        }
        this.bombs = new ArrayList<>(Collections.unmodifiableList(Objects.requireNonNull(bombs)));
        this.explosions = new ArrayList<>(Collections.unmodifiableList(Objects.requireNonNull(explosions)));
        this.blasts = new ArrayList<>(Collections.unmodifiableList(Objects.requireNonNull(blasts)));
    }

    /**
     * Secondary constructor to facilitate creating a game at it's beginning.
     *
     * @param board
     *            The board on which the game will be played.
     * @param players
     *            The four players that will play the game.
     */
    public GameState(Board board, List<Player> players) {
        this(0, board, players, new ArrayList<Bomb>(),
                new ArrayList<Sq<Sq<Cell>>>(), new ArrayList<Sq<Cell>>());
    }

    /**
     * Get the next Gamestate in function of the current state and player
     * actions.
     *
     * @param speedChangeEvents
     *            External interaction with the game from the players.
     * @param bombDropEvents
     *            The players that want to drop a bomb at the current tick.
     * @return Returns a new GameState based on the game's current state and
     *         different player actions.
     */
    public GameState next(Map<PlayerID, Optional<Direction>> speedChangeEvents,
            Set<PlayerID> bombDropEvents) {
        // First the blasts evolve.
        List<Sq<Cell>> blasts1 = nextBlasts(this.blasts, this.board,
                this.explosions);

        // From which we derive the next blasted cells
        Set<Cell> blastedCells1 = blastedCells(blasts1);

        // Then find out what bonuses were consumed, to create the board.
        Set<Cell> consumedBonuses = nextConsumedBonuses(this.players,
                this.board);

        // Now we have all our info, we create the next board.
        Board board1 = nextBoard(this.board, consumedBonuses, blastedCells1);

        // We next calculate how the explosions will evolve.
        List<Sq<Sq<Cell>>> explosions1 = nextExplosions(this.explosions);

        // Let's get the bombs newly dropped, passing players sorted in good
        // order.
        List<Player> sortedPlayers = this.players.stream()
                .sorted(conflictSolver(this.ticks))
                .collect(Collectors.toList());
        List<Bomb> bombs01 = newlyDroppedBombs(sortedPlayers, bombDropEvents,
                this.bombs);

        // Now all the bombs evolve:
        List<Bomb> bombs11 = new ArrayList<>();
        // all currently dropped bombs.
        bombs01.addAll(this.bombs);

        for (Bomb mine : bombs01) {
            Sq<Integer> fuse = mine.fuseLengths().tail();
            if (fuse.isEmpty() || blastedCells1.contains(mine.position())) {
                explosions1.addAll(mine.explosion());
            } else {
                // We want to rebuild a bomb with the same characteristics but a
                // shorter fuselength.
                bombs11.add(new Bomb(mine.ownerId(), mine.position(), fuse,
                        mine.range()));
            }
        }

        // Start gathering information to create the next players
        Map<PlayerID, Bonus> playerBonuses1 = nextPlayerBonuses(this.players,
                this.board, consumedBonuses, this.ticks);

        List<Player> players1 = nextPlayers(this.players, playerBonuses1,
                bombedCells(bombs11).keySet(), this.board, blastedCells1,
                speedChangeEvents);

        // Now that we finally have all our necessary information, we can create
        // the next board.
        return new GameState(this.ticks + 1, board1, players1, bombs11,
                explosions1, blasts1);
    }

    /**
     * Get the number of passed ticks.
     *
     * @return The number of elapsed ticks.
     */
    public int ticks() {
        return this.ticks;
    }

    /**
     * Gets if the game is over or not.
     *
     * @return A boolean indicating if the game is finished or not.
     */
    public boolean isGameOver() {
        return this.ticks > Ticks.TOTAL_TICKS || this.alivePlayers().size() < 2;
    }

    /**
     * Gets the remaining time before the game ends.
     *
     * @return The number of ticks before the end of the game.
     */
    public double remainingTime() {
        return (Ticks.TOTAL_TICKS - this.ticks) / (double)Ticks.TICKS_PER_SECOND;
    }

    /**
     * Gets an optional containing either the player that won the game if he
     * exists, or nothing.
     *
     * @return An optional containing the winning player if he exists or an
     *         empty optional otherwise.
     */
    public Optional<PlayerID> winner() {
        // Store the alive players since we will use them twice
        List<Player> alive = this.alivePlayers();
        return Optional
                .ofNullable(alive.size() == 1 ? alive.get(0).id() : null);
    }

    /**
     * Get the game's board.
     *
     * @return The game's board.
     */
    public Board board() {
        return this.board;
    }

    /**
     * Get the game's players.
     *
     * @return The game's players.
     */
    public List<Player> players() {
        return this.players;
    }

    /**
     * Get's the game's players that have at least one life left.
     *
     * @return The game's alive players, i.e. the players with at least one life
     *         left.
     */
    public List<Player> alivePlayers() {
        return this.players.stream().filter(Player::isAlive)
                .collect(Collectors.toList());
    }

    /**
     * Get a mapping of Cells to Bombs; used to easily check if a Cell has been
     * bombed or not.
     *
     * @return A map linking the cells on which they have been dropped to the
     *         dropped bombs.
     */
    public Map<Cell, Bomb> bombedCells() {
        return bombedCells(this.bombs);
    }

    /*
     * Redefinition of the above method to allow getting the bombedCells for
     * different states, the current one as well as the future one.
     */
    private static Map<Cell, Bomb> bombedCells(List<Bomb> bombs1) {
        return bombs1.stream()
                .collect(Collectors.toMap(Bomb::position, Function.identity()));
    }

    /**
     * Get a Set of Cells that are currently subject to explosive weather.
     *
     * @return A set of cells that have explosion particles (blasts) on them.
     */
    public Set<Cell> blastedCells() {
        return blastedCells(this.blasts);
    }

    /*
     * Redefinition of the above method to allow getting blastedCells for
     * different states, the current one as well as the future one.
     */
    private static Set<Cell> blastedCells(List<Sq<Cell>> blasts1) {
        return blasts1.stream()
                .collect(Collectors.mapping(Sq::head, Collectors.toSet()));
    }

    /*
     * Calculate the next blasts based on current blasts and new ones generated
     * by explosions on the board.
     */
    private static List<Sq<Cell>> nextBlasts(List<Sq<Cell>> blasts0,
            Board board0, List<Sq<Sq<Cell>>> explosions0) {
        List<Sq<Cell>> output = new ArrayList<Sq<Cell>>();

        // We will first consider the already existing blasts.
        // Using a foreach protects us elegantly against empty lists, but we'll
        // still protect against nulls... because better safe than sorry.

        for (Sq<Cell> blast0 : Objects.requireNonNull(blasts0)) {

            // Check that the blast does not naturally die or hit an unfree
            // block.
            if (!Objects.requireNonNull(blast0).tail().isEmpty()
                    && board0.blockAt(blast0.head()).isFree()) {
                output.add(blast0.tail());
            }
        }

        // Now we will generate the additional blasts that are created by active
        // explosions.
        for (Sq<Sq<Cell>> explosion0 : Objects.requireNonNull(explosions0)) {

            if (!Objects.requireNonNull(explosion0).isEmpty()) {
                output.add(explosion0.head());
            }
        }
        return output;
    }

    /*
     * Calculate the state of the board for the next tick using the existing
     * board and events that can change it: explosions and bonuses that have
     * been consumed.
     */
    private static Board nextBoard(Board board0, Set<Cell> consumedBonuses,
            Set<Cell> blastedCells1) {
        List<Sq<Block>> newBoard = new ArrayList<>();

        for (Cell tile : Cell.ROW_MAJOR_ORDER) {
            Block currentBlock = board0.blockAt(tile);
            // When a bonus is consumed we put a forever free tile afterwards.
            
            if (consumedBonuses.contains(tile)) {
                newBoard.add(Sq.constant(Block.FREE));

                // Deal with Cells that have been blasted by an explosion.
            } else if (blastedCells1.contains(tile)) {

                // If the cell was a destructible wall, we want to make it
                // crumble and then replace it by either a bonus either
                // a free tile.
                if (currentBlock.equals(Block.DESTRUCTIBLE_WALL)) {

                    // Check if we give a bonus or not
                    int luck = NOTSORANDOM.nextInt(3);
                    Block afterWall;
                    switch (luck) {
                    case 0:
                        afterWall = Block.BONUS_BOMB;
                        break;
                    case 1:
                        afterWall = Block.BONUS_RANGE;
                        break;
                    case 2:
                        afterWall = Block.FREE;
                        break;
                    default:
                        throw new Error();
                    }

                    // Replace the wall with a new sequence containing a
                    // crumbling wall for the specified number of ticks, and a
                    // mystery block.
                    newBoard.add(Sq
                            .repeat(Ticks.WALL_CRUMBLING_TICKS,
                                    Block.CRUMBLING_WALL)
                            .concat(Sq.constant(afterWall)));

                    // If the cell contains a bonus, we must check that it is
                    // not disappearing and if it is not, then we make it so.
                } else if (currentBlock.isBonus()) {
                    newBoard.add(board0.blocksAt(tile)
                            .limit(Ticks.BONUS_DISAPPEARING_TICKS + 1)
                            .concat(Sq.constant(Block.FREE)).tail());

                } else {
                    // Do nothing if the tile is indestructible or free.
                    newBoard.add(board0.blocksAt(tile).tail());
                }

            } else {
                // Do nothing if the tile is not affected by an event.
                newBoard.add(board0.blocksAt(tile).tail());
            }

        }
        return new Board(newBoard);
    }

    /*
     * Get the state of on-board explosions at the next tick.
     */
    private static List<Sq<Sq<Cell>>> nextExplosions(
            List<Sq<Sq<Cell>>> explosions0) {
        return explosions0.stream().filter(e -> !e.isEmpty())
                .collect(Collectors.mapping(Sq::tail, Collectors.toList()));
    }

    /*
     * Deal with bonuses must be consumed this turn for the next tick.
     */
    private static Set<Cell> nextConsumedBonuses(List<Player> players0,
            Board board0) {
        // Each player's position for easy searching
        List<SubCell> playerPositions = players0.stream()
		.map(Player::position)
                .collect(Collectors.toList());
        return Cell.ROW_MAJOR_ORDER.stream()
                .filter(c -> board0.blockAt(c).isBonus() && playerPositions
                        .contains(SubCell.centralSubCellOf(c)))
                .collect(Collectors.toSet());
    }

    /*
     * Associates each of the above-calculated consumed bonuses with the player
     * that found it.
     */
    private static Map<PlayerID, Bonus> nextPlayerBonuses(List<Player> players0,
            Board board0, Set<Cell> consumedBonus, int ticks) {
        Map<PlayerID, Bonus> output = new HashMap<>();

        // Find players that consume a bonus.
        Map<Cell, Optional<Player>> luckyGuys = players0.stream()
                .filter(Player::isAlive)
                // check if the player is on bonused cell.
                .filter(p -> consumedBonus
                        .contains(p.position().containingCell()))
                // get the player with highest priority order.
                .collect(Collectors.groupingBy(
                        p -> p.position().containingCell(), Collectors.maxBy((p,
                                q) -> conflictSolver(ticks).compare(p, q))));

        for (Cell bonus : luckyGuys.keySet()) {
            output.put(luckyGuys.get(bonus).get().id(),
                    board0.blockAt(bonus).associatedBonus());
        }
        return output;
    }

    /*
     * Checks that players that want to drop bombs are aloud to.
     */
    private static List<Bomb> newlyDroppedBombs(List<Player> players0,
            Set<PlayerID> bombDropEvents, List<Bomb> bombs0) {
        // We fetch the positions of already dropped bombs to forbid
        // double-dropping and we create a map of each player to the number of
        // bombs he has dropped. We could have used the bombedCell() method but
        // then we would have uselessly iterated twice on the list of bombs...
        Set<Cell> bombedCells0 = new HashSet<>();
        Map<PlayerID, Integer> bombCounter = new HashMap<>();
        List<Bomb> newBombs = new ArrayList<>();

        // We need to initialise each counter to make sure our compute function
        // runs.
        for (PlayerID pid : PlayerID.values()) {
            bombCounter.put(pid, 0);
        }

        // Accounting for already dropped bombs.
        for (Bomb mine : bombs0) {
            bombedCells0.add(mine.position());
            bombCounter.compute(mine.ownerId(), (id, count) -> ++count);
        }

        for (Player player : players0) {
            if (player.isAlive() && bombDropEvents.contains(player.id())
                    && bombCounter.get(player.id()) < player.maxBombs()
                    && !bombedCells0
                            .contains(player.position().containingCell())) {
                Bomb newBomb = player.newBomb();
                newBombs.add(newBomb);
                // store the position in the list of forbidden positions to
                // prevent conflicts
                // (we have been passed the players in conflict solving order).
                bombedCells0.add(newBomb.position());
            }
        }

        return newBombs;
    }

    /*
     * Calculate the next states (position, life ...) of the next players for
     * the next ticks.
     */
    private static List<Player> nextPlayers(List<Player> players0,
            Map<PlayerID, Bonus> playerBonuses, Set<Cell> bombedCells1,
            Board board1, Set<Cell> blastedCells1,
            Map<PlayerID, Optional<Direction>> speedChangeEvents) {

        List<Player> toRet = new ArrayList<>();
        Optional<Direction> empty = Optional.empty();

        for (Player p : players0) {
            // We first calculate the next DirectedPos sequence of the player.
            // (if
            // there is new indication)
            Sq<DirectedPosition> nextDirectedPos;

            if (speedChangeEvents.containsKey(p.id())) {
                Sq<DirectedPosition> toNextCentral = p.directedPositions()
                        .takeWhile(pos -> !pos.position().isCentral());

                if (speedChangeEvents.get(p.id()) != empty) {
                    // If their is a new Direction asked in the map
                    // speedChangeEvents, we put it in the variable askedDir.
                    Direction askedDir = speedChangeEvents.get(p.id()).get();

                    if (askedDir.isParallelTo(p.direction())) {
                        // If the direction asked is parallel to the Player
                        // Direction, we change directly his sequence of
                        // DirectedPositions.
                        nextDirectedPos = DirectedPosition
                                .moving(p.directedPositions().head()
                                        .withDirection(askedDir));

                    } else {
                        // If the direction isn't parallel, we make a new
                        // DirectedPosition sequence :
                        // - The player go to the next central of cell he's
                        // moving to.
                        // - He turn and move in the Direction given
                        SubCell nextCentral = p.directedPositions()
                                .findFirst(pos -> pos.position().isCentral())
                                .position();

                        nextDirectedPos = toNextCentral.concat(
                                DirectedPosition.moving(new DirectedPosition(
                                        nextCentral, askedDir)));
                    }

                } else {
                    // If the Direction given is empty, then we stop the Player
                    // to the next central of a cell he's moving to.
                    DirectedPosition atNextCentral = p.directedPositions()
                            .findFirst(pos -> pos.position().isCentral());
                    SubCell nextCentral = atNextCentral.position();

                    nextDirectedPos = toNextCentral.concat(
                            DirectedPosition.stopped(new DirectedPosition(
                                    nextCentral, atNextCentral.direction())));
                }

            } else {
                // If there is no new indication, the player keep the same
                // sequence of DirectedPositions.
                nextDirectedPos = p.directedPositions();
            }

            // We verify if the player can move on his next directed position.

            // The player is blocked by a wall iff :
            // - he is at a central Subcell of a Cell.
            // - there is a wall (at the next tick) in the direction the Player
            // is facing.
            boolean blockByWall = p.position().isCentral()
                    && !board1
                            .blocksAt(
                                    nextDirectedPos.head().position()
                                            .containingCell()
                                            .neighbor(nextDirectedPos.head()
                                                    .direction()))
                            .tail().head().canHostPlayer();

            // The player is block by a bomb only if :
            // - He's on a Cell occupied by a bomb.
            // - He's at a distance of 6 to the bomb. (distance to the central
            // Subcell).
            // - He's going to the central of the cell.
            boolean blockByBomb = bombedCells1.contains(
                    nextDirectedPos.head().position().containingCell())
                    && nextDirectedPos.head().position()
                            .distanceToCentral() == 6
                    && nextDirectedPos.tail().head().position()
                            .distanceToCentral() < p.position()
                                    .distanceToCentral();

            if (p.lifeState().canMove() && !(blockByBomb || blockByWall)) {
                nextDirectedPos = nextDirectedPos.tail();
            }

            // We analyze the next LifeState of the player.
            // If the player is vulnerable and is on a blasted Cell, his
            // lifestates sequence is modified, otherwise we tail his LifeState
            // sequence.
            Sq<LifeState> nextLife;
            if (p.lifeState().state() == LifeState.State.VULNERABLE
                    && blastedCells1.contains(nextDirectedPos.head().position()
                            .containingCell())) {
                nextLife = p.statesForNextLife();
            } else {
                nextLife = p.lifeStates().tail();
            }

            // We apply bonuses to the player if he's on the map, otherwise we
            // don't.
            if (playerBonuses.containsKey(p.id())) {
                toRet.add(
                        playerBonuses.get(p.id())
                                .applyTo(new Player(p.id(), nextLife,
                                        nextDirectedPos, p.maxBombs(),
                                        p.bombRange())));
            } else {
                toRet.add(new Player(p.id(), nextLife, nextDirectedPos,
                        p.maxBombs(), p.bombRange()));
            }

        }

        return toRet;
    }

    /*
     * Returns a comparator that can be used to solve conflicts by 'comparing'
     * two players according to precedence.
     */
    private static Comparator<Player> conflictSolver(int ticks) {
        List<PlayerID> solveOrder = GameState.permutations.get(ticks % 24);
        return (p1,
                p2) -> solveOrder.indexOf(p1.id()) < solveOrder.indexOf(p2.id())
                        ? 1 : -1;
    }
}
