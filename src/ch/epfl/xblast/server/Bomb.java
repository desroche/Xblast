package ch.epfl.xblast.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ch.epfl.cs108.Sq;
import ch.epfl.xblast.ArgumentChecker;
import ch.epfl.xblast.Cell;
import ch.epfl.xblast.Direction;
import ch.epfl.xblast.PlayerID;

/**
 * A bomb that can be dropped by a player, explode, and 
 * cause severe injury to others and self.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class Bomb {
    /*
     * The identity of the player who dropped the bomb.
     */
    private final PlayerID owner;

    /*
     * The bomb's position on the board.
     */
    private final Cell position;

    /*
     * The length, in ticks, between the instant when the bomb is dropped and
     * it's explsion.
     */
    private final Sq<Integer> fuseLengths;

    /*
     * The number of cells around the bomb that the explosion will engulf.
     */
    private final int range;

    /**
     * Creates a new bomb after checking the passed arguments are correct.
     *
     * @param ownerId
     *            The bomb's owner's PlayerID.
     * @param position
     *            The bomb's position on the board.
     * @param fuseLengths
     *            The bomb's current and future fuselengths.
     * @param range
     *            The number of cells around the bomb that it's explosion will
     *            engulf.
     */
    public Bomb(PlayerID ownerId, Cell position, Sq<Integer> fuseLengths,
            int range) {

        this.owner = Objects.requireNonNull(ownerId);
        this.position = Objects.requireNonNull(position);
        this.range = ArgumentChecker.requireNonNegative(range);

        if (Objects.requireNonNull(fuseLengths).isEmpty()) {
            throw new IllegalArgumentException(
                    "fuseLengths must not be empty!");
        } else {
            this.fuseLengths = fuseLengths;
        }
    }

    /**
     * Creates a new bomb with a fuseLength passed as an int.
     *
     * @param ownerId
     *            The bomb's owner's PlayerID.
     * @param position
     *            The bomb's position on the board.
     * @param fuseLength
     *            The number of ticks before the bomb explodes.
     * @param range
     *            The number of cells around the bomb that it's explosion will
     *            engulf.
     */
    public Bomb(PlayerID ownerId, Cell position, int fuseLength, int range) {
        this(ownerId, position,
                Sq.iterate(fuseLength, i -> (i - 1)).limit(fuseLength),
                range);
    }

    /**
     * Get the bomb's owner.
     *
     * @return The bomb's owner's playerID.
     */
    public PlayerID ownerId() {
        return owner;
    }

    /**
     * Get the bomb's position.
     *
     * @return The cell containing the bomb.
     */
    public Cell position() {
        return position;
    }

    /**
     * Get the bomb's fuselengths.
     *
     * @return The sequence containing the bomb's fuselengths.
     */
    public Sq<Integer> fuseLengths() {
        return fuseLengths;
    }

    /**
     * Get the bomb's fuse current length.
     *
     * @return The number of ticks before the bomb's explosion.
     */
    public int fuseLength() {
        return fuseLengths.head();
    }

    /**
     * Get the bomb's range.
     *
     * @return The number of cells that will be affected by it's explosion.
     */
    public int range() {
        return range;
    }

    /**
     * Get the bomb's explosion under the form of a vector containing the
     * explosion's four arms.
     *
     * @return The bomb's explosion.
     */
    public List<Sq<Sq<Cell>>> explosion() {
        List<Sq<Sq<Cell>>> output = new ArrayList<Sq<Sq<Cell>>>();
        for (Direction cardinal : Direction.values()) {
            output.add(explosionArmTowards(cardinal));
        }
        return output;
    }

    /*
     * Is used to create one arm of the explosion to facilitate explosion().
     */
    private Sq<Sq<Cell>> explosionArmTowards(Direction dir) {
        return Sq.repeat(Ticks.EXPLOSION_TICKS,
                Sq.iterate(position, c -> c.neighbor(dir)).limit(range));
    }
}
