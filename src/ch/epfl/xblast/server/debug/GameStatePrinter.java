package ch.epfl.xblast.server.debug;

import java.util.List;

import ch.epfl.xblast.Cell;
import ch.epfl.xblast.server.Block;
import ch.epfl.xblast.server.Board;
import ch.epfl.xblast.server.GameState;
import ch.epfl.xblast.server.Player;

public final class GameStatePrinter {
    private GameStatePrinter() {}

    private final static String std = "\u001b[m";
    //private final static String red = "\u001b[31m";
    private final static String cyan = "\u001b[36m";
    private final static String white = "\u001b[37m";
    //private final static String black = "\u001b[30m";
    //private final static String yellow = "\u001b[33m";

    //private final static String bg_std = "\u001b[m";
    private final static String bg_red = "\u001b[41m";
    //private final static String bg_cyan = "\u001b[46m";
    private final static String bg_white = "\u001b[47m";
    private final static String bg_black = "\u001b[40m";
    private final static String bg_yellow = "\u001b[43m";

    private final static String blink = "\u001b[5m";
    private final static String unblink = "\u001b[25m";
    
    //private final static String redraw = "\u001b[2J";
    
    private final static String bomb = bg_yellow + "\u001b[31mÒ." + std;
    private final static String explosion = bg_yellow + "\u001b[35m++" + std;

    public static void printGameState(GameState s) {
        List<Player> ps = s.alivePlayers();
        Board board = s.board();
        //System.out.println(redraw);
        for (int y = 0; y < Cell.ROWS; ++y) {
            xLoop: for (int x = 0; x < Cell.COLUMNS; ++x) {
                Cell c = new Cell(x, y);
                for (Player p: ps) {
                    if (p.position().containingCell().equals(c)) {
                        System.out.print(stringForPlayer(p));
                        continue xLoop;
                    }
                }
		Block b = board.blockAt(c);
                if (b == Block.INDESTRUCTIBLE_WALL || b == Block.DESTRUCTIBLE_WALL | b == Block.CRUMBLING_WALL) {
                    System.out.print(stringForBlock(b));
                } else if (s.bombedCells().containsKey(c)) {
                    System.out.print(bomb);
                } else if (s.blastedCells().contains(c)) {
                    System.out.print(explosion);
                } else {
                    System.out.print(stringForBlock(b));
                }
            }
            System.out.println();
        }
        
        for(Player p: ps){
            System.out.println(p.id() + " , " + p.position().containingCell().toString() + " , " + p.position().distanceToCentral());
            System.out.println(p.lives() + " , " + p.lifeState().state());
            System.out.println("Attribut : Bombs : " + p.maxBombs() + ", Range  :" + p.bombRange());
        }
    }

    private static String stringForPlayer(Player p) {
        StringBuilder b = new StringBuilder();
	switch (p.lifeState().state()) {
		case DYING: 
		case INVULNERABLE:
			b.append(blink);
			break;
		default:
			break;
	}
	b.append(cyan);
        b.append(p.id().ordinal() + 1);
        switch (p.direction()) {
        	case N: b.append('^'); break;
        	case E: b.append('>'); break;
        	case S: b.append('v'); break;
        	case W: b.append('<'); break;
        }
	b.append(unblink);
	b.append(std);
        return b.toString();
    }

    private static String stringForBlock(Block b) {
        switch (b) {
		case FREE: return bg_white + "  " + std;
		case INDESTRUCTIBLE_WALL: return bg_black + white + "##" + std;
		case DESTRUCTIBLE_WALL: return "??";
		case CRUMBLING_WALL: return blink + "¿¿" + unblink;
		case BONUS_BOMB: return bg_red + "+b" + std; 
		case BONUS_RANGE: return bg_red + "+r";
		default: throw new Error();
        }
    }
}
