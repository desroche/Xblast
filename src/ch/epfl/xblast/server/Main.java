package ch.epfl.xblast.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.StandardProtocolFamily;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Map.Entry;

import ch.epfl.xblast.Direction;
import ch.epfl.xblast.PlayerAction;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.server.painters.BoardPainter;

/**
 * The main class representing the server of the game.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public class Main {
    /**
     * The main method, used to start the server.
     * @param args  Number of players (If no args, set the number of players to 4).
     * @throws IOException
     * @throws InterruptedException 
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        
        // 1) WAITING FOR THE PLAYERS.
        int numberOfPlayers = argsReader(args);
        
        // Configure the reception channel.
        DatagramChannel channel = DatagramChannel.open(StandardProtocolFamily.INET);
        channel.configureBlocking(true);
        channel.bind(new InetSocketAddress(2016));
        Map<SocketAddress, PlayerID> joinedPlayers = joinedPlayersing(channel, numberOfPlayers);
        
        
        // 2) WE INITIALIZE THE GAME.
        GameState gStateServ = Level.DEFAULT_LEVEL.getState();
        BoardPainter boardPainter = Level.DEFAULT_LEVEL.getPainter();
        System.out.println("Starting the game!");
        
        long timeStarted = System.nanoTime();
        long actualTime;
        long timeToNextTick;
        
        // 3) RUN THE GAME.
        while(!gStateServ.isGameOver()){
            // 3.1) SEND THE SERIELIZED GAME TO THE CONNECTED CLIENTS.
            
            List<Byte> toSend = GameStateSerializer.serialize(gStateServ, boardPainter);
            
            for(Entry<SocketAddress, PlayerID> e : joinedPlayers.entrySet()){
                // toSend.size for the serialized Gamestate and 1 for the PlayerID. 
                ByteBuffer bBuffToSend = ByteBuffer.allocate(toSend.size() + 1);
                bBuffToSend.put((byte)e.getValue().ordinal());
                for(Byte b : toSend){
                    bBuffToSend.put(b.byteValue());
                }
                bBuffToSend.flip();
                channel.send(bBuffToSend, e.getKey());               
            }
            actualTime = System.nanoTime();
            timeToNextTick = timeStarted + (long)Ticks.TICK_NANOSECOND_DURATION * (long)(gStateServ.ticks()) - actualTime ;
            if(timeToNextTick > 0){
                // If we must wait, we compute the time the server has to "sleep" (in milliseconds + the precision in nano seconds)
                Thread.sleep(timeToNextTick / 1_000_000l ,(int)(timeToNextTick % 1_000_000));
            }
            
            // 3.2) RECEIVE THE INFORMMATION SENT BY THE CLIENTS
            channel.configureBlocking(false);
            Map<PlayerID, Byte> actionsAsked = actionsAsked(channel, joinedPlayers);
            
            // 3.3) TRANSCRIPT ALL THE RECEIVED INFORMATION INTO MAPS.
            Map<PlayerID, Optional<Direction>> speedChangeEvents = new HashMap<>();
            Set<PlayerID> bombDropEvents = new HashSet<>();
            
            for(Entry<PlayerID, Byte> e : actionsAsked.entrySet()){
                switch(e.getValue().byteValue()){
                case (byte)1:
                    speedChangeEvents.put(e.getKey(), Optional.of(Direction.N));
                    break;
                case (byte)2:
                    speedChangeEvents.put(e.getKey(), Optional.of(Direction.E));
                    break;
                case (byte)3:
                    speedChangeEvents.put(e.getKey(), Optional.of(Direction.S));
                    break;
                case (byte)4:
                    speedChangeEvents.put(e.getKey(), Optional.of(Direction.W));
                    break;
                case (byte)5:
                    speedChangeEvents.put(e.getKey(), Optional.empty());
                    break;
                case (byte)6:
                    bombDropEvents.add(e.getKey());
                    break;
                default:
                    //We do nothing. Like John Snow.
                }
            }
            
            //3.4 COMPUTE THE NEXT STATE OF THE GAME.
            gStateServ = gStateServ.next(speedChangeEvents, bombDropEvents);
        }
        
        // 4) DISPLAy THE WINNER
        if(gStateServ.alivePlayers().size() == 1){
            System.out.println(gStateServ.winner().get());
        }
        
        // 5) CLOSE THE CHANNEL AND END THE GAME
        channel.close();
    }
    
    /**
     * Private method used to read the number of players.
     * @param args Number of players (If no args, set the number of players to 4).
     * @return The number of player (ash an int)
     */
    private static int argsReader(String[] args){
        final int CHAR_LOCATION_OF_0_IN_UNICODE = 48;
        if (args.length == 0 || args == null){
            return 4;
        }else{
            return (int)args[0].charAt(0) - CHAR_LOCATION_OF_0_IN_UNICODE;
        }
    }
    
    /**
     * Private method used to add the client who want to join the game.
     * @param channel The DatagramChannel used by the client to join (received information)
     * @param numberOfPlayers The number of player we want to join.
     * @return The Map that link SocketAddress to PlayerID.
     * @throws IOException
     */
    private static Map<SocketAddress, PlayerID> joinedPlayersing(DatagramChannel channel, int numberOfPlayers) throws IOException{
        
        Map<SocketAddress, PlayerID> joinedPlayers = new HashMap<>();
        ByteBuffer tempJoined = ByteBuffer.allocate(1);
        
        while(joinedPlayers.size() != numberOfPlayers){
            SocketAddress joined = channel.receive(tempJoined);
            if(tempJoined.get(0) == (byte)PlayerAction.JOIN_GAME.ordinal() && !joinedPlayers.containsKey(joined)){
                joinedPlayers.put( joined , PlayerID.values()[joinedPlayers.size()] );
                System.out.println(PlayerID.values()[joinedPlayers.size() - 1] + " joined");
                
                tempJoined.clear();
            }else{
                tempJoined.clear();
            }
        }
        
        return joinedPlayers;
    }
    
    /**
     * Private method used to receive the action asked by the clients.
     * @param channel The Datagramchannel used by the clients to send messages.
     * @param joinedPlayers The Map that links SocketAddresses to the PlayerIDs.
     * @return a Map that associates to each PlayerID the Byte representing the action the corresponding player wants to perform.
     * @throws IOException
     */
    private static Map<PlayerID, Byte> actionsAsked(DatagramChannel channel, Map<SocketAddress, PlayerID> joinedPlayers) throws IOException{
        
        ByteBuffer receivedInfo = ByteBuffer.allocate(1);
        SocketAddress s;
        Map<PlayerID, Byte> actionsAsked = new HashMap<>();
        while((s = channel.receive(receivedInfo)) != null){
            if(joinedPlayers.containsKey(s)){
                actionsAsked.put(joinedPlayers.get(s), receivedInfo.get(0));
                receivedInfo.clear();
            }else{
                receivedInfo.clear();
            }
        }
        
        return actionsAsked;
    }
}
