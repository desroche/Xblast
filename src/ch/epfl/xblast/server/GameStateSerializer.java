package ch.epfl.xblast.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ch.epfl.xblast.Cell;
import ch.epfl.xblast.Direction;
import ch.epfl.xblast.RunLengthEncoder;
import ch.epfl.xblast.server.painters.BoardPainter;
import ch.epfl.xblast.server.painters.ExplosionPainter;
import ch.epfl.xblast.server.painters.PlayerPainter;

/**
 * A non instanciable class used to serialize a GameState.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class GameStateSerializer {
    /*
     * GameStateSerializer isn't an instanciable class.
     */
    private GameStateSerializer() {}
    
    /**
     * Static method used to serialize a GameState (the board, the explosions, the bombs, the players and the remaning time).
     * @param s The gamestate we went to serialize.
     * @param b The BoardPainter we use to paint the Board.
     * @return A List of Bytes, corresponding to the serialize game.
     */
    public static List<Byte> serialize(GameState s, BoardPainter b){
        List<Byte> toRet = new ArrayList<>();
        List<Byte> boardByte = new ArrayList<>();
        
        // Get the byte for each Block, and then add the compressed List of Bytes received
        Board boa = s.board();
        for(Cell c : Cell.SPIRAL_ORDER){
            boardByte.add(b.byteForCell(boa, c));
        }
        List<Byte> encodedBoard = RunLengthEncoder.encoder(boardByte);
        toRet.add((byte)encodedBoard.size());
        toRet.addAll(encodedBoard);
        
        // Get the byte for each blasted cell and bomb.
        List<Byte> expBombByte = new ArrayList<>();
        Set<Cell> blasted = s.blastedCells();
        Map<Cell, Bomb> bombed = s.bombedCells();
        for(Cell c : Cell.ROW_MAJOR_ORDER){
            //We verify if there is a blast on the Cell c and if we can draw it (is a free block).
            if(blasted.contains(c)  && boa.blockAt(c).isFree()){
                boolean isW = blasted.contains(c.neighbor(Direction.W));
                boolean isS = blasted.contains(c.neighbor(Direction.S));
                boolean isE = blasted.contains(c.neighbor(Direction.E));
                boolean isN = blasted.contains(c.neighbor(Direction.N));
                expBombByte.add( ExplosionPainter.byteForBlast(isW, isS, isE, isN) );
            } else if(bombed.containsKey(c)){
                    //If there is no blast, there can be a bomb.
                    expBombByte.add( ExplosionPainter.byteForBomb(bombed.get(c)) );
            } else {
                // If ther is none of those, we add the "Empty" one.
                expBombByte.add( ExplosionPainter.BYTE_FOR_EMPTY );
            }
        }
        // Compress the explosions and bombs data.
        List<Byte> encodedExpl = RunLengthEncoder.encoder(expBombByte);
        toRet.add((byte)encodedExpl.size());
        toRet.addAll(encodedExpl);
        
        // Then we add the players.
        List<Byte> playersByte = new ArrayList<>();
        for(Player p : s.players()){
            playersByte.add((byte)p.lives());
            playersByte.add((byte)p.position().x());
            playersByte.add((byte)p.position().y());
            playersByte.add(PlayerPainter.byteForPlayer(s.ticks(), p));
        }
        toRet.addAll(playersByte);
        
        // Then we add the remaining time.
        toRet.add((byte)Math.ceil(s.remainingTime()/2));
        
        return toRet;
    }
}
