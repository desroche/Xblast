package ch.epfl.xblast.server;

import ch.epfl.xblast.Time;

/**
 * The Ticks interface defines the time different events of the game take in
 * game ticks.
 *
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public interface Ticks {
    /**
     * The length, in ticks, of a player's death.
     */
    public static final int PLAYER_DYING_TICKS = 8;

    /**
     * The length, in ticks, of a player's invulnerable state following his
     * respawn.
     */
    public static final int PLAYER_INVULNERABLE_TICKS = 64;

    /**
     * The length, in ticks, between the dropping of a bomb and it's explosion.
     */
    public static final int BOMB_FUSE_TICKS = 100;

    /**
     * The length, in ticks, of a bomb's explosion.
     */
    public static final int EXPLOSION_TICKS = 30;

    /**
     * The time, in ticks, it takes for a wall to crumble. Equal to the length
     * of an explosion.
     */
    public static final int WALL_CRUMBLING_TICKS = EXPLOSION_TICKS;

    /**
     * The time, in ticks, it takes for a bonus hit by an explosion to
     * diseappear. Equal to the length of an explosion.
     */
    public static final int BONUS_DISAPPEARING_TICKS = EXPLOSION_TICKS;

    /**
     * The numbers of Ticks per seconds.
     */
    public static final int TICKS_PER_SECOND = 20;

    /**
     * The number of nano seconds in one tick.
     */
    public static final int TICK_NANOSECOND_DURATION = Time.NS_PER_S / TICKS_PER_SECOND ;

    /**
     * The total numbers of Ticks in a game.
     */
    public static final int TOTAL_TICKS = TICKS_PER_SECOND * Time.S_PER_MIN * 2;
}
