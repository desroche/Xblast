package ch.epfl.xblast.server;

import ch.epfl.xblast.server.GameState;
import ch.epfl.xblast.server.painters.BoardPainter;

import java.util.Objects;

/**
 * Represents a level of the game, composed of a GameState giving the data of the game,
 * and a BoardPainter giving the graphic style of the level.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class Level {
    private final BoardPainter bPainter;
    private final GameState gState;

    // The default level.
    public static final Level DEFAULT_LEVEL = new Level(BoardPainter.DEFAULT_PAINTER, GameState.DEFAULT_GAMESTATE);

    /**
     * Constructor used to create a game in it's initial form.
     *
     * @param bPainter
     *  The BoardPainter that will define how the board looks like.
     * @param gState
     *  The intial state of the game.
     */
    public Level(BoardPainter bPainter, GameState gState) {
        // As BoardPainter and GameState are immuable classes, we can directly copy them here.
        this.bPainter = Objects.requireNonNull(bPainter);
        this.gState = Objects.requireNonNull(gState);
    }

    /**
     * Accessor for the level's painter.
     *
     * @return This level's painter.
     */
    public BoardPainter getPainter() {
        return this.bPainter;
    }

    /**
     * Accessor for the level's GameState.
     *
     * @return This level's GameState.
     */
    public GameState getState() {
        return this.gState;
    }
}
