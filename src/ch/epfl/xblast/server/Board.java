package ch.epfl.xblast.server;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ch.epfl.cs108.Sq;
import ch.epfl.xblast.Cell;
import ch.epfl.xblast.Lists;

/**
 * This class represents a game board.
 * 
 * The game board is stored as a one-dimensional list of block sequences, stored
 * in row major order.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 * @see Block
 */
public final class Board {
    /*
     * A one-dimensional list of each block's possible states in sequence form,
     * the blocks being ordered in row major order.
     */
    private final List<Sq<Block>> currentBoard;

    /*
     * The board's horizental length.
     */
    private static final int LENGTH = 15;

    /*
     * The board's vertical length.
     */
    private static final int HEIGHT = 13;

    /**
     * The default board.
     */
    public static final Board DEFAULT_BOARD = ofQuadrantNWBlocksWalled(createDefault());

    /**
     * The constructor builds a board from a given list of each block sequence.
     *
     * @param blocks
     *            The row-major ordered list of each block's possible states, as
     *            a sequence.
     * @throws IllegalArgumentException
     *             Will throw an exception if the number of blocks given is
     *             incorrect to build a Board.
     */
    public Board(List<Sq<Block>> blocks) throws IllegalArgumentException {
        if (blocks == null || blocks.size() != (LENGTH * HEIGHT)) {
            throw new IllegalArgumentException("The passed blocks are not correct; I can't build a board with them!");
        }
	// Deep copy and immutable.
        currentBoard = Collections.unmodifiableList(new ArrayList<>(blocks));
    }

    /**
     * Builds a board of constant state blocks from a list of 13 rows, each
     * containing 15 blocks.
     *
     * @param rows
     *            A list of 13 rows, each containing 15 blocks.
     * @throws IllegalArgumentException
     *             An IllegalArgumentException will be thrown if the number of
     *             rows or blocks per row is incorrect.
     * @return A 16 x 14 Board.
     */
    public static Board ofRows(List<List<Block>> rows)
            throws IllegalArgumentException {
        // check that there are enough given rows.
        if (rows == null || rows.size() != HEIGHT) {
            throw new IllegalArgumentException(
                    "You must provide exactly " + HEIGHT + "rows!");
        } else {
            List<Sq<Block>> newBlocks = new ArrayList<Sq<Block>>();
            for (List<Block> row : rows) {
                // check that each row has enough blocks to build the board.
                if (row == null || row.size() != LENGTH) {
                    throw new IllegalArgumentException(
                            "Each row must contain exactly " + LENGTH
                                    + " elements!");
                } else {
                    for (Block block : row) {
                        // As we iterate on rows then blocks of said rows, we
                        // effectively add here constant sequences of blocks in
                        // row major order to the board.
                        newBlocks.add(Sq.constant(block));
                    }
                }
            }
            return new Board(newBlocks);
        }
    }

    /**
     * Creates a board with all four edges walled, from a list of 11 rows, each
     * containing exactly 13 blocks.
     *
     * @param innerBlocks
     *            The list of rows that make up the inner blocks of the board.
     * @throws IllegalArgumentException
     *             An IllegalArgumentException will be thrown if the number of
     *             rows or blocks is incorrect (respectively 11 and 13).
     * @return A 16 x 14 Board with all four edges walled.
     */
    public static Board ofInnerBlocksWalled(List<List<Block>> innerBlocks)
            throws IllegalArgumentException {
        // As we are lining the board with walls, we check that we have enough
        // rows to create the inner part of the board.
        if (innerBlocks == null || innerBlocks.size() != (HEIGHT - 2)) {
            throw new IllegalArgumentException(
                    "You must provide exactly 11 inner rows!");
        } else {
            List<Sq<Block>> newBlocks = new ArrayList<Sq<Block>>();
            // Add the first row of indestructible walls that corresponds
            // to the top wall.
            for (int i = 0; i < LENGTH; i++) {
                newBlocks.add(Sq.constant(Block.INDESTRUCTIBLE_WALL));
            }
            // iterate on given rows
            for (List<Block> row : innerBlocks) {
                // check that we are given the correct number of blocks to
                // create the inner part of the board.
                if (row == null || row.size() != (LENGTH - 2)) {
                    throw new IllegalArgumentException(
                            "Each inner row must contain exactly "
                                    + (LENGTH - 2) + " elements!");
                } else {
                    // add the first block of each row that will make up the
                    // left wall
                    newBlocks.add(Sq.constant(Block.INDESTRUCTIBLE_WALL));
                    // iterate on blocks of given row
                    for (Block block : row) {
                        newBlocks.add(Sq.constant(block));
                    }
                    // add the last block of each row that will make up the
                    // right wall
                    newBlocks.add(Sq.constant(Block.INDESTRUCTIBLE_WALL));
                }
            }
            // Add the last row of indestructible walls that corresponds
            // to the bottom wall.
            for (int i = 0; i < LENGTH; i++) {
                newBlocks.add(Sq.constant(Block.INDESTRUCTIBLE_WALL));
            }
            return new Board(newBlocks);
        }
    }

    /**
     * Creates a walled board with a horizontal and a vertical symmetry axis,
     * using only the North-West quandrant of the desired board.
     *
     * @param quadrantNWBlocks
     *            The list of half rows that make up the inner part of the
     *            North- West quadrant of the board we want to build.
     * @return A 16 x 14 Board with all four edges walled.
     */
    public static Board ofQuadrantNWBlocksWalled(
            List<List<Block>> quadrantNWBlocks)
            throws IllegalArgumentException {
        // Check that we have enough rows to infer the whole board:
	double goodHeight = Math.ceil((HEIGHT - 2) / 2.0);
	double goodLength = Math.ceil((LENGTH - 2) / 2.0);
        if (quadrantNWBlocks == null
                || quadrantNWBlocks.size() != goodHeight) {
            throw new IllegalArgumentException(
                    "You must provide exactly " + goodHeight
                            + " inner rows for a quadrantNW board build.");
        } else {
            // We will now create the lists that will correspond to the other
            // three quadrants:
            // - keep row order and reverse block order for NE
            // - reverse row order and keep block order for SW
            // - reverse row order and reverser block order for SE

            List<List<Block>> innerBlocks = new ArrayList<List<Block>>();
            // Northern half
            for (List<Block> row : quadrantNWBlocks) {
                // As this is the first call that iterates on rows, we will
                // check they have the correct length here.
                if (row == null
                        || row.size() != goodLength) {
                    throw new IllegalArgumentException(
                            "Each inner row must have exactly "
                                    + goodLength
                                    + " blocks for a quadrantNW board build.");
                } else {
                    innerBlocks.add(Lists.mirrored(row));
                }
            }
            return ofInnerBlocksWalled(Lists.mirrored(innerBlocks));
        }
    }

    /**
     * Returns the sequence of possible states for the block at the given cell.
     *
     * @param c
     *            The cell for which we want to obtain the block sequence.
     * @return The sequence of blocks that are the possible block states for the
     *         given Cell.
     */
    public Sq<Block> blocksAt(Cell c) {
        return currentBoard.get(c.rowMajorIndex());
    }

    /**
     * Returns the head of the possible block states at the given cell, which is
     * equivalent to obtaining the current block at given cell.
     *
     * @param c
     *            The cell for which we want to know the current block.
     * @return The current block at the given Cell.
     */
    public Block blockAt(Cell c) {
        return blocksAt(c).head();
    }

    /*
     * Create the quadrant needed for the default board.
     */
    private static List<List<Block>> createDefault() {
	Block __ = Block.FREE;
        Block XX = Block.INDESTRUCTIBLE_WALL;
        Block xx = Block.DESTRUCTIBLE_WALL;
        return Arrays.asList(
            Arrays.asList(__, __, __, __, __, xx, __),
            Arrays.asList(__, XX, xx, XX, xx, XX, xx),
            Arrays.asList(__, xx, __, __, __, xx, __),
            Arrays.asList(xx, XX, __, XX, XX, XX, XX),
            Arrays.asList(__, xx, __, xx, __, __, __),
            Arrays.asList(xx, XX, xx, XX, xx, XX, __));
	    
    }
}
