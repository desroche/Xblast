package ch.epfl.xblast.server;

import java.util.Objects;
import java.util.List;
import java.util.Arrays;

import ch.epfl.cs108.Sq;
import ch.epfl.xblast.ArgumentChecker;
import ch.epfl.xblast.Cell;
import ch.epfl.xblast.Direction;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.SubCell;
import ch.epfl.xblast.server.Player.LifeState.State;

/**
 * Players of the game as well as the methods used to 
 * get their states, lives and so on...
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 * @see Player.LifeState Player.DirectedPosition
 */

public final class Player {
    /*
     * The ID of the Player (PLAYER_1, ... , PLAYER_4)
     */
    private final PlayerID id;
    
    /*
     * The Sequence of LifeStates of the Player (the head of the sequence represents the Player's current LifeState).
     */
    private final Sq<LifeState> lifeStates;
    
    /*
     * The Sequence of DirectedPosition of the Player (the head of the sequence represents the Player's current DirectedPosition). 
     */
    private final Sq<DirectedPosition> directedPos;
    
    /*
     * The maximum number of Bombs the player can set.
     */
    private final int maxBombs;
    
    /*
     * The range of the explosion of the Bombs the player can set.
     */
    private final int bombRange;

    /**
     * A list containg the four default players in a 'start-of-game' configuration.
     */
    public static final List<Player> DEFAULT_PLAYERS = getDefaultPlayers();

    /**
     * Construct a new Player
     * 
     * @param id
     *            The PlayerID of the player (1, 2, 3 ...)
     * @param lifeStates
     *            Current and future LifeStates of the player.
     * @param directedPos
     *            The sequence of the player's positions and directions.  
     * @param maxBombs
     *            The maximum bombs the Player can set on the ground
     * @param bombRange
     *            The number of cells this player's explosions span.
     * @throws IllegalArgumentException
     *             If the maxBombs or bomb Range are strictly negative.
     * @throws NullPointerException
     *             If one of the sequences are null objects.
     */
    public Player(PlayerID id, Sq<LifeState> lifeStates,
            Sq<DirectedPosition> directedPos, int maxBombs, int bombRange)
                    throws IllegalArgumentException, NullPointerException {
        this.id = Objects.requireNonNull(id);
        this.lifeStates = Objects.requireNonNull(lifeStates);
        this.directedPos = Objects.requireNonNull(directedPos);
        this.maxBombs = ArgumentChecker.requireNonNegative(maxBombs);
        this.bombRange = ArgumentChecker.requireNonNegative(bombRange);
    }

    /**
     * Another way to build a Player.
     * 
     * @param id
     *            The PlayerID of the Player (1, 2, 3 ...)
     * @param lives
     *            How many lives the Player has.
     * @param position
     *            The Players position in the subcell referential.
     * @param maxBombs
     *            The maximum number of bombs that the Player can set on the 
     *            ground.
     * @param bombRange
     *            The number of cells this player's bombs span.
     * @throws IllegalArgumentException
     *             If the maxBombs, bombRange or lives are strictly negative
     * @throws NullPointerException
     *             If the position is null.
     */
    public Player(PlayerID id, int lives, Cell position, int maxBombs,
            int bombRange)
                    throws IllegalArgumentException, NullPointerException {
        this(id, lifeStatesConstructer(ArgumentChecker.requireNonNegative(lives)),
                DirectedPosition.stopped(new DirectedPosition(
                        SubCell.centralSubCellOf(
                                Objects.requireNonNull(position)),
                        Direction.S)),
                maxBombs, bombRange);
    }

    /**
     * Private method that build a Sq List of LifeState for the second builder.
     * 
     * @param lives
     *            The number of lives of the player.
     * @return The correct sequence of LifeState (Invulnerable then Vulnerable).
     * @throws IllegalArgumentException
     *             if the number of lives  is negative or null.
     */
    private static Sq<LifeState> lifeStatesConstructer(int lives){
        if (lives == 0) {
            //The Player is already dead.
            return Sq.constant(new LifeState(lives, State.DEAD));
        } else {
            //We build a new sequence, composed of an invulnerable state and then a vulnerable state.
            LifeState toRetInv = new LifeState(lives,
                    State.INVULNERABLE);
            LifeState toRetVul = new LifeState(lives,
                    State.VULNERABLE);
            return Sq.repeat(Ticks.PLAYER_INVULNERABLE_TICKS, toRetInv)
                    .concat(Sq.constant(toRetVul));
        }
    }

    /**
     * Get the Player's id.
     * 
     * @return PlayerID , the id of the player.
     */
    public PlayerID id() {
        return id;
    }

    /**
     * Get the lifeState Sequence of the player.
     * 
     * @return The Sequence lifeStates of the Player.
     */
    public Sq<LifeState> lifeStates() {
        return lifeStates;
    }

    /**
     * Get the current LifeState of the Player.
     * 
     * @return The player's current lifestate.
     */
    public LifeState lifeState() {
        return lifeStates.head();
    }

    /**
     * Get a new sequence of lifestates after a death, i.e. with one less life.
     * 
     * @return A sequence of LifeStates representing the new future of the 
     *         player.
     */
    public Sq<LifeState> statesForNextLife() {
        // First, the Player is in a dying State. 
        LifeState dying = new LifeState(lives(), State.DYING);
        Sq<LifeState> toRet = Sq.repeat(Ticks.PLAYER_DYING_TICKS, dying);
        // We can call the lifeStatesConstructer with a fewer life.
        return toRet.concat(lifeStatesConstructer(lives() - 1));
    }

    /**
     * Get the number of lives of the Player
     * 
     * @return the number of lives of the Player
     */
    public int lives() {
        return lifeState().lives;
    }

    /**
     * Get a boolean, if the the Player is alive or not
     * 
     * @return A boolean indicating if the player is alive. 
     */
    public boolean isAlive() {
        return lives() > 0;
    }

    /**
     * Get the DirectedPosition sequence of the Player
     * 
     * @return A sequence of the current and future directed positions of the player.
     */
    public Sq<DirectedPosition> directedPositions() {
        return this.directedPos;
    }

    /**
     * Get the current position.
     * 
     * @return the current position of the Player.
     */
    public SubCell position() {
        return this.directedPos.head().position();
    }

    /**
     * Get the current Direction of the Player.
     * 
     * @return the Direction of the Player.
     */
    public Direction direction() {
        return this.directedPos.head().direction();
    }

    /**
     * Get the number of maximum bombs that the player can set on the Board.
     * 
     * @return The maximum number of bombs this player can set.
     */
    public int maxBombs() {
        return this.maxBombs;
    }

    /**
     * Construct and return a Player with a new number of maximum possible bombs.
     * 
     * @param newMaxBombs
     *            The new number of bombs that the Player can set on the Board.
     * @return a new Player with a new number of maximum bombs.
     */
    public Player withMaxBombs(int newMaxBombs) {
        return new Player(this.id, this.lifeStates, this.directedPos,
                newMaxBombs, this.bombRange);
    }

    /**
     * Get the range of the bombs the Player can set.
     * 
     * @return The range of the bombs the Player can set.
     */
    public int bombRange() {
        return this.bombRange;
    }

    /**
     * Construct and return a new Player with a new range for his bombs.
     * 
     * @param newBombRange
     *            The new range of the bomb's explosion.
     * @return a new Player with a new bomb range.
     */
    public Player withBombRange(int newBombRange) {
        return new Player(this.id, this.lifeStates, this.directedPos,
                this.maxBombs, newBombRange);
    }

    /**
     * Return a bomb that the player sets on the ground.
     * 
     * @return a new Bomb with the coordinates of the player and all the
     *         information needed
     */
    public Bomb newBomb() {
        return new Bomb(this.id, this.position().containingCell(),
                Ticks.BOMB_FUSE_TICKS, bombRange);
    }

    /**
     * The class that represents the Life and the State of a Player.
     * 
     * @author Joachim Desroches (257178)
     * @author Benoit Juif (261829)
     * @see Player
     */
    public final static class LifeState {
        
        /*
         * The number of remaining lives of a Player.
         */
        private final int lives;
        
        /*
         * The current State of a Player.
         */
        private final State state;

        /**
         * Construct a new LifeState, a class that hold the number of lives of the
         * Player and his current State
         * 
         * @param lives
         *            The numbers of lives of the player (must be positive)
         * @param state
         *            The current State of the Player (Invulnerable ...) Must not
         *            be null.
         * @throws IllegalArgumentException
         *             If lives isn't positive
         * @throws NullPointerException
         *             If state is null.
         */
        public LifeState(int lives, State state)
                throws IllegalArgumentException, NullPointerException {
            this.lives = ArgumentChecker.requireNonNegative(lives);
            this.state = Objects.requireNonNull(state);
        }

        /**
         * The differents states of the Player are stocked in this Enumeration
         * 
         * @author Joachim Desroches (257178)
         * @author Benoit Juif (261829)
         * @see Player
         */
        public enum State {
            INVULNERABLE, VULNERABLE, DYING, DEAD;
        }

        /**
         * Return the number of lives of the LifeState.
         * 
         * @return the int lives of the LifeState.
         */
        public int lives() {
            return lives;
        }

        /**
         * Return the State of the object LifeState
         * 
         * @return the State (vulnerable ...) of the current LifeState.
         */
        public State state() {
            return state;
        }

        /**
         * Return a boolean if the Player can move depending his state
         * 
         * @return a boolean depending of the State. (Invulnerable or
         *         Vulnerable).
         */
        public boolean canMove() {
            return (state == State.INVULNERABLE || state == State.VULNERABLE);
        }

    }

    /**
     * The class that represent the position and the Direction of a Player.
     * 
     * @author Joachim Desroches (257178)
     * @author Benoit Juif (261829)
     * @see Player
     */
    public final static class DirectedPosition {
        
        /**
         * The current position of the Player (stocked in a DirectedPosition).
         */
        private final SubCell position;
        
        /**
         * The current direction of the Player (stocked in a DirectedPosition).
         */
        private final Direction direction;

        /**
         * Construct a new Object of the class DirectedPosition
         * 
         * @param position
         *            The player's location.
         * @param direction
         *            The Direction in which the player is looking.
         */
        public DirectedPosition(SubCell position, Direction direction)
                throws NullPointerException {
            this.position = Objects.requireNonNull(position);
            this.direction = Objects.requireNonNull(direction);
        }

        /**
        * Get the player's position.
         * 
         * @return the player's position.
         */
        public SubCell position() {
            return position;
        }

        /**
         * Get the player's direction.
         * 
         * @return The direction of the player.
         */
        public Direction direction() {
            return direction;
        }

        /**
         * Create a new DirectedPosition with the same Direction but a new
         * Position.
         * 
         * @param newPosition
         *            The location of the new DirectedPosition.
         * @return a new DirectedPosition with the same direction but a
         *         different position.
         */
        public DirectedPosition withPosition(SubCell newPosition) {
            return new DirectedPosition(newPosition, this.direction);
        }

        /**
         * Create a new DirectedPosition with the same Position but a new
         * Direction.
         * 
         * @param newDirection
         *            The new Direction we want to set for the player.
         * @return a new DirectedPosition with the same position but with a
         *         different Direction.
         */
        public DirectedPosition withDirection(Direction newDirection) {
            return new DirectedPosition(this.position, newDirection);
        }

        /**
         * Create a sequence of DirectedPositions representing a stopped player.
         * @param p
         *         The player's position whilst he is immobile.
         * @return a constant DirectedPosition sequence
         */
        public static Sq<DirectedPosition> stopped(DirectedPosition p) {
            return Sq.constant(p);
        }

        /**
         * Construct and return a DirectedPosition sequence that evolves in the given
         * Direction.
         * 
         * @param p
         *         The starting DirectedPosition, oriented in the direction the player will follow.
         * @return The evolving DirectedPosition Sequence.
         */
        public static Sq<DirectedPosition> moving(DirectedPosition p) {
            return Sq.iterate(p, directedPos -> directedPos
                    .withPosition(directedPos.position.neighbor(p.direction)));
        }
    }

    /*
     * Creation of 4 default players as usually at the start of a game.
     */
    private static List<Player> getDefaultPlayers() {
	   return Arrays.asList(new Player(PlayerID.PLAYER_1, 3, new Cell(1, 1), 2, 3),
                new Player(PlayerID.PLAYER_2, 3, new Cell(-2, 1), 2, 3),
                new Player(PlayerID.PLAYER_3, 3, new Cell(-2, -2), 2, 3),
                new Player(PlayerID.PLAYER_4, 3, new Cell(1, -2), 2, 3));
    }
}
