package ch.epfl.xblast.server;

/**
 * Describes the different types of bonus we can encounter in the game.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 * @see Block
 */
public enum Bonus {
   INC_BOMB {
        public final static int MAX_BOMBS = 9;
        
      /**
       * Increases the number of bombs a player can drop, taking into account
       * the maximum of 9 given 
       * @return Player with new settings (more bombs)
       */
      @Override
      public Player applyTo(Player player) {
          
          if (player.maxBombs() >= MAX_BOMBS) {
              return player;
          } else {
              return player.withMaxBombs(player.maxBombs() + 1);
          }
      }
    },

    INC_RANGE {
        public final static int MAX_RANGE = 9;
      /**
       * Return a new Player with a greater range for his bombs
       * taking into account the max of 9 given by the rules.
       * @return Player with new settings (more bomb range)
       */
      @Override
      public Player applyTo(Player player) {
          if (player.bombRange() >= MAX_RANGE) {
              return player;
          } else {
              return player.withBombRange(player.bombRange() + 1);
          }
      }
    };

    abstract public Player applyTo(Player player);
  }
