package ch.epfl.xblast;

/**
 * The four cardinal directions that will be used in the game.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public enum Direction {
    N, E, S, W;

    /**
     * Returns the opposite of the current direction.
     *
     * @return The opposite of the current direction.
     */
    public Direction opposite() throws IllegalArgumentException {
        switch (this) {
        case N:
            return S;
        case E:
            return W;
        case S:
            return N;
        case W:
            return E;
        // This case should never happen, but better safe than sorry.
	// After all, the matrix is only that well coded...
        default:
            throw new IllegalArgumentException("Unknown direction.");
        }
    }

    /**
     * Checks if the current direction is horizontal or vertical.
     * 
     * @return True if the current direction is horizontal, false otherwise.
     */
    public boolean isHorizontal() {
        return ((this == E) || (this == W));
    }

    /**
     * Checks if the current direction is parallel to the one passed in
     * argument.
     * 
     * @param that
     *            The direction that we will want to compare the current one
     *            with.
     * @return True if that and the current direction are parallel, false
     *         otherwise.
     */
    public boolean isParallelTo(Direction that) {
        return (this == that || this == that.opposite());
    }
}
