package ch.epfl.xblast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The Cell class represents one tile on the game board.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class Cell {
    public final static int COLUMNS = 15;
    public final static int ROWS = 13;
    public final static int COUNT = COLUMNS * ROWS;

    private final int x;
    private final int y;

    public static final List<Cell> ROW_MAJOR_ORDER = Collections
            .unmodifiableList(rowMajorOrder());
    public static final List<Cell> SPIRAL_ORDER = Collections
            .unmodifiableList(spiralOrder());

    /**
     * Create a new Cell with the given coordinates.
     * 
     * @param x
     *            Horizental position of the cell
     * @param y
     *            Vertical position of the cell
     */
    public Cell(int x, int y) {
        this.x = Math.floorMod(x, COLUMNS);
        this.y = Math.floorMod(y, ROWS);
    }

    /**
     * Get the current horizental position of the cell.
     * 
     * @return The cell's horizental position.
     */
    public int x() {
        return x;
    }

    /**
     * Get the current vertical position of the cell.
     * 
     * @return The cell's vertical position.
     */
    public int y() {
        return y;
    }

    /**
     * Get the row major order index of the cell.
     * 
     * @return The row major order index of the cell.
     **/
    public int rowMajorIndex() {
        return x + y * COLUMNS;
    }

    /**
     * Get origin's neighbor in the passed direction.
     * 
     * @param dir
     *            Direction of the neighbor asked.
     * @return origin's neighbor in the passed direction.
     * @see ch.epfl.xblast.Direction
     */
    public Cell neighbor(Direction dir) {
        switch (dir) {
        case N:
            return new Cell(x, y - 1);
        case S:
            return new Cell(x, y + 1);
        case E:
            return new Cell(x + 1, y);
        case W:
            return new Cell(x - 1, y);
        }

        return null;
    }

    /*
     * Method used to create a list of the game's board's
     * Cells ordered in Row Major Order.
     */
    private static ArrayList<Cell> rowMajorOrder() {
        ArrayList<Cell> rowOrd = new ArrayList<Cell>();
        for (int row = 0; row < ROWS; ++row) {
            for (int column = 0; column < COLUMNS; ++column) {
                rowOrd.add(new Cell(column, row));
            }
        }
        return rowOrd;
    }

    /*
     * Method used to create a list of the game's board's
     * Cells in Spiral Order.
     */
    private static ArrayList<Cell> spiralOrder() {
        ArrayList<Cell> spiOrd = new ArrayList<Cell>();
        ArrayList<Integer> ix = new ArrayList<Integer>();
        for (int column = 0; column < COLUMNS; ++column) {
            ix.add(column);
        }
        ArrayList<Integer> iy = new ArrayList<Integer>();
        for (int row = 0; row < ROWS; ++row) {
            iy.add(row);
        }
        boolean horizontal = true;

        while (!ix.isEmpty() && !iy.isEmpty()) {
            // We set in two different arrays the horizontal line and the
            // numbers of columns of the Board.
            List<Integer> i1;
            List<Integer> i2;
            if (horizontal) {
                i1 = ix;
                i2 = iy;
            } else {
                i1 = iy;
                i2 = ix;
            }

            // We add to the List spiOrd the cell of the horizontal line, and
            // then we delete it and delete the first column.
            Integer c2 = i2.get(0);
            i2.remove(0);
            for (int i = 0; i < i1.size(); ++i) {
                Cell c;
                if (horizontal) {
                    c = new Cell(i1.get(i), c2);
                    spiOrd.add(c);
                } else {
                    c = new Cell(c2, i1.get(i));
                    spiOrd.add(c);
                }
            }
            // Once this is done, we turn the board of 45° and we start again.
            Collections.reverse(i1);
            horizontal = !horizontal;
        }

        return spiOrd;
    }

    /**
     * Override of Object's equal method. Two cells are equal if they have the same
     * position.
     *
     * @param that
     *     The object we are comparing to origin for equality.
     * 
     * @return True if origin and that are both cells and have the same position, false otherwise.
     */
    @Override
    public boolean equals(Object that) {
        
        return (that != null 
                && (that instanceof Cell) 
                && (((Cell) that).x() == this.x && ((Cell) that).y() == this.y));
    }

    /**
     * Get a textual representation of the cell.
     * 
     * @return a String of the form (x, y) for the Cell.
     */
    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }
    
    /**
     * Override Object's hashcode method to give a better hashcode for each Cell (Row major index position)
     * 
     * @return a hashcode unique to each set of equal Cells.
     */
    @Override
    public int hashCode(){
        return rowMajorIndex();
    }
}
