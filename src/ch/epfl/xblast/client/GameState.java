package ch.epfl.xblast.client;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.SubCell;

/**
 * Class used to represents a GameState, client side.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class GameState {
    
    /*
     * The total List of Players of the game.
     */
    private final List<Player> players;
    
    /*
     * The list of Images used to represent the Board.
     */
    private final List<Image> imgBoard;
    
    /*
     * The list of Images used to represent bombs and explosions.
     */
    private final List<Image> imgBombsExplo;
    
    /*
     * The list of Images used to represent the scoreline.
     */
    private final List<Image> imgScores;
    
    /*
     * The list of Images used to represent the timeline. 
     */
    private final List<Image> imgTime;
    
    /**
     * Construct a new client-side GameState.
     *
     * @param players 
     * 	        The list of players of the game.
     * @param imgBoard 
     * 	        The list of images representing the Board.
     * @param imgBombsExplo 
     * 	        The list of images representing the bombs and the explosions on the Board.
     * @param imgScores 
     * 	        The list of images used to represent the score of the game.
     * @param imgTime 
     * 	        The list of images used to represent the remaining time in the game.
     * @throws NullPointerException 
     *          If one of the lists is null.
     */
    public GameState(List<Player> players, List<Image> imgBoard, List<Image> imgBombsExplo, List<Image> imgScores, List<Image> imgTime) throws NullPointerException {
	// Several sanity checks going on...
        this.players = Collections.unmodifiableList(new ArrayList<>(Objects.requireNonNull(players)));
        this.imgBoard = Collections.unmodifiableList(new ArrayList<>(Objects.requireNonNull(imgBoard)));
        this.imgBombsExplo = Collections.unmodifiableList(new ArrayList<>(Objects.requireNonNull(imgBombsExplo)));
        this.imgScores = Collections.unmodifiableList(new ArrayList<>(Objects.requireNonNull(imgScores)));
        this.imgTime = Collections.unmodifiableList(new ArrayList<>(Objects.requireNonNull(imgTime)));
    }
    
    /**
     * Get the list of players.
     * @return The list of players.
     */
    public List<Player> players() {
        return players;
    }
    
    /**
     * Get the list of images reprenting the Board.
     * @return The list of images of the board.
     */
    public List<Image> imgBoard() {
        return imgBoard;
    }
    
    /**
     * Get the list of images representing the explosions.
     * @return The list of images representing explosions.
     */
    public List<Image> imgBombsExplosions() {
        return imgBombsExplo;
    }
    
    /**
     * Get the list of images representing the the scores.
     * @return The images of the scores.
     */
    public List<Image> imgScores() {
        return imgScores;
    }
    
    /**
     * Get the list of images representing the time left.
     * @return The images of the timeline.
     */
    public List<Image> imgTime() {
        return imgTime;
    }
    
    /**
     * Class used to represent a Player, client-side.
     * 
     * @author Joachim Desroches (257178)
     * @author Benoit Juif (261829)
     */
    public static class Player {
        
        /*
         *  The ID of the Player.
         */
        private PlayerID id;
        
        /*
         * The number of lives of the Player.
         */
        private int lives;
        
        /*
         * The position of the Player.
         */
        private SubCell position;
        
        /*
         * The graphical representation of the Player.
         */
        private Image img;
        
        /**
         * Construct a new client-side player. 
         * @param id 
         *      The id of the player.
         * @param lives 
         *      The number of remaning lives of the player.
         * @param position 
         *      The position of the player.
         * @param img 
         *      The image used to represent the player.
         */
        public Player(PlayerID id, int lives, SubCell position, Image img) {
           this.id = id;
           this.lives = lives;
           this.position = position;
           this.img = img;
        }
        
        /**
         * Get the id of the Player
         * @return the id of the current Player.
         */
        public PlayerID id() {
            return id;
        }
        
        /**
         * Get the number of lives of the player.
         * @return the restant lives of the player.
         */
        public int lives() {
            return lives;
        }
        
        /**
         * Get the position of the player.
         * @return the position.
         */
        public SubCell position() {
            return position;
        }
        
        /**
         * Get the corresponding image of the Player, obtain by his position and his id.
         * @return the img of the Player.
         */
        public Image img() {
            return img;
        }
    }
}
