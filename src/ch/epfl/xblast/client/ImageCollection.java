package ch.epfl.xblast.client;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;

import javax.imageio.ImageIO;

/**
 * Class used to locate and use the images for the client.
 *
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class ImageCollection {
    
    /*
     * The name of the repository we need to look into.
     */
    private final String nameRep;
    
    /*
     * The Map used to find the Image corresponding to the integer at the beggining of the name of the image.
     */
    private final Map<Integer, Image> mapIntToImg;
    
    /**
     * Create a new ImageCollection from the content of a directory.
     *
     * @param nameRep
     * 		The repository we are indexing.
     * @throws NullPointerException If the String given in parameter is null.
     */
    public ImageCollection(String nameRep) throws NullPointerException {
        this.nameRep = Objects.requireNonNull(nameRep);
        this.mapIntToImg = new HashMap<>();
        
	// We put in a map the image, associated to the first three caracters in it's
	// name, that we know will be integers.
        try {
            File dir = new File(ImageCollection.class
                    .getClassLoader()
                    .getResource(this.nameRep)
                    .toURI());
            for(File f : dir.listFiles()){
                Integer numberIm = Integer.parseInt(f.getName().substring(0, 3));
                try {
                    mapIntToImg.put(numberIm, ImageIO.read(f));
		} catch (IOException e) {
                    System.err.print("An IOException was thrown whilst loading the images!");
                    e.printStackTrace();
		}
            }
        } catch (URISyntaxException e) {
            System.err.print("A URISyntaxException was thrown whilst loading the images!");
            e.printStackTrace();
        }
    }
    
    /**
     * Get the image associated with the index passed as a parameter.
     *
     * @param index
     *          The index of the image we want to find. It should be the first
     *          three integers of it's name.
     * @return The image with the given index if found.
     * @throws NoSuchElementException If no image corresponds to the given index.
     */
    public Image image(int index) throws NoSuchElementException {
        if (mapIntToImg.containsKey(index)) {
            return mapIntToImg.get(index);
        } else {
            throw new NoSuchElementException("The index is wrong/or there is no such image");
        }
    }
    
    /**
     * Get the image associated with the index passed as a parameter, or null if it does not exist.
     * @param index
     *          The index of the image we want to find. It should be the first three digits of the image's name.
     * @return the image corresponding to the index, or null if there is no such image.
     */
    public Image imageOrNull(int index) {
        if (mapIntToImg.containsKey(index)) {
            return mapIntToImg.get(index);
        } else {
            return null;
        }
    }
}
