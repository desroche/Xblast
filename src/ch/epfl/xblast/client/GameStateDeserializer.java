package ch.epfl.xblast.client;

import ch.epfl.xblast.RunLengthEncoder;
import ch.epfl.xblast.ArgumentChecker;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.SubCell;
import ch.epfl.xblast.Cell;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.awt.Image;

/**
 * This class extracts a GameState from a sequence of bytes.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class GameStateDeserializer {
    // class constants
    private final static int BOARD_SIZE = 195;
    private final static int NUM_PLAYERS = 4;
    private final static int PLAYER_SRLZED = NUM_PLAYERS * 4;
    private final static int GAME_DURATION = 120;

    private final static ImageCollection IMGCO_PLAYERS = new ImageCollection("player");
    private final static ImageCollection IMGCO_TIME = new ImageCollection("score");
    private final static ImageCollection IMGCO_SCORES = new ImageCollection("score");
    private final static ImageCollection IMGCO_BOARD = new ImageCollection("block");
    private final static ImageCollection IMGCO_EXPL = new ImageCollection("explosion");
    
    // This class is not instanciable.
    private GameStateDeserializer() {}

    /**
     * Static method used to deserialize a list of Bytes into a client version of the GameState.
     * @param input 
     * The list of Byte we want to deserialize.
     * @return the client GameState that correspond the input.
     */
    public static GameState deserializeGameState(List<Byte> input) {
        // Because of the compression, the size of the list we receive is variable, so we'll store it.
        int inpSize = input.size();
        // The sizes of different sections are stored as headers to these sections, so we extract them first.
        int boardStart  = 1;
        int boardStop   = Byte.toUnsignedInt(input.get(0)) + 1;
        // As we will use subLists, we add one to take into account that the upper bound is exclusive.
        int boomStart   = boardStop + 1;
        int boomStop    = boomStart + Byte.toUnsignedInt(input.get(boardStop));

	// Seperate each group of information.
        List<Image> imgBoard = extract(RunLengthEncoder.decoder(input.subList(boardStart, boardStop)), IMGCO_BOARD, true);
        List<Image> imgBoom  = extract(RunLengthEncoder.decoder(input.subList(boomStart, boomStop)), IMGCO_EXPL, false);
        List<GameState.Player> players = extractPlayers(input.subList(boomStop, boomStop + PLAYER_SRLZED + 1));
        List<Image> imgTime = createTime(input.get(inpSize - 1));
        List<Image> imgScores  = createScores(players);

        return new GameState(players, imgBoard, imgBoom, imgScores, imgTime);
    }

    // Extract the images of board or bombs in the serialized data.
    private static List<Image> extract(List<Byte> serialIn, ImageCollection imgs, boolean spiral) {
        Image[] output = new Image[BOARD_SIZE];

        // obtain a copy of the board in spiral order to re-order correctly the blocks.
        List<Cell> board = Cell.SPIRAL_ORDER;

        int rowOrd;
        for (int i = 0; i < Cell.COUNT; i++) {
            rowOrd = !spiral ? i : board.get(i).rowMajorIndex();
            output[rowOrd] = imgs.imageOrNull(ArgumentChecker.requireNonNegative((int) serialIn.get(i)));
        }
            return Arrays.asList(output);
    }

    // Create the players in the client's format.
    private static List<GameState.Player> extractPlayers(List<Byte> serialIn) {
        List<GameState.Player> output = new ArrayList<>();

        // Fields that we will use.
        PlayerID id;
        SubCell position;
        int lives;
        Image pic;
        
        // Four fields per player.
        for (int i = 0; i < PLAYER_SRLZED; i += 4) {
            id = PlayerID.values()[i / 4];
            lives = Byte.toUnsignedInt(serialIn.get(i));
            position = new SubCell(Byte.toUnsignedInt(serialIn.get(i + 1)), Byte.toUnsignedInt(serialIn.get(i + 2)));
            pic = IMGCO_PLAYERS.imageOrNull(Byte.toUnsignedInt(serialIn.get(i + 3)));
            output.add(new GameState.Player(id, lives, position, pic));
        }

        return output;
    }

    // Create the list of images representing the time.
    private static List<Image> createTime(Byte inputTime) {
        int remaining = Byte.toUnsignedInt(inputTime);
        List<Image> output = new ArrayList<>();

        output.addAll(Collections.nCopies(remaining, IMGCO_TIME.image(21)));
        output.addAll(Collections.nCopies((GAME_DURATION / 2) - remaining, IMGCO_TIME.image(20)));

        return output;
    }

    // Create the list of images representing the player's scores.
    private static List<Image> createScores(List<GameState.Player> players) {
        // Players were added in order to the players list so we can iterate on them in order.
	List<Image> output = new ArrayList<>();
        int img = 0;
        for (GameState.Player pl : players) {
            // Add the player dead or alive.
            output.add(IMGCO_SCORES.image( pl.lives() > 0 ? img : img + 1 ));

            // Add the middle and right images.
            output.add(IMGCO_SCORES.image(10));
            output.add(IMGCO_SCORES.image(11));
            img += 2;
        }
	// Add the padding in the middle of the four players.
        for (int i = 0; i < 8; i++) {
            output.add(6, IMGCO_SCORES.image(12));
        }
	return output;
    }
}
