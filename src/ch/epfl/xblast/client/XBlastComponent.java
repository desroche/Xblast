package ch.epfl.xblast.client;

import javax.swing.JComponent;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.Cell;


/**
 * Swing component showing the state of an XBlast game.
 *
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class XBlastComponent extends JComponent {
    // Game constants.
    private final int WIDTH = 960;
    private final int HEIGHT = 688;

    // The gamestate we're drawing.
    private GameState gState = null;
    // Fow whom are we drawing.
    private PlayerID pov = null;

    //Functions we used to calculate where we draw the players.
    private final static Function<Integer, Integer> FX = (a -> 4*a - 24);
    private final static Function<Integer, Integer> FY = (b -> 3*b - 52);
    
    // We create 2 comparators to know which player we should draw in front of the other, depending on for whom we are drawing.
    // We first check if p1 is at a lower position (bigger y) than p2.
    private static final Comparator<GameState.Player> COMPY = (p1, p2) -> {
        if(p1.position().y() == p2.position().y())
            return 0;
        return p1.position().y() > p2.position().y() ? 1 : -1; 
    };

    // And if the players are at the same Y-position, we draw the player played by the client in front.
    private final Comparator<GameState.Player> compID = (p1, p2) -> {
        //If they are the same, no problem.
        if(p1.id() == p2.id()){
            return 0;
        }else{
            //If one of p1 or p2 is the pov, then he's drawn last.
            if(pov == p1.id() || pov == p2.id()){
                return pov == p1.id() ? 1 : -1; 
            }else{
                /* If not, we compare the position of the players with their ordinals:
                 * We split the players in two groups using their position compared to the POV.
                 * If they are in two different groups, then we must reverse the natural order;
                 * if however  they are in the same group, then natural order applies.
                 */
                if(p1.id().ordinal() < pov.ordinal() && p2.id().ordinal() > pov.ordinal()) {return 1;}
                else if(p1.id().ordinal() > pov.ordinal() && p2.id().ordinal() < pov.ordinal()) {return -1;}
                else {return p1.id().ordinal() > p2.id().ordinal() ? 1 : -1;}
            }
        }
    };
    
    // We concatenate both of those comparator in one.
    private final Comparator<GameState.Player> finalComp = COMPY.thenComparing(compID);
        
    /**
     * Default constructor.
     */
    public XBlastComponent() {
            super();
    }

    /**
     * Returns the ideal size of the gameboard.
     *
     * @return The ideal size of the gameboard.
     */
    @Override
    public Dimension getPreferredSize() {
            return new Dimension(WIDTH, HEIGHT);
    }

    /**
     * Paint the current gamestate.
     *
     * @param graphX0
     *     The graphic component in which we will paint our gamestate.
     */
    @Override
    protected void paintComponent(Graphics graphX0) {
        // Set the correct context.
        Graphics2D graphX = (Graphics2D) graphX0;

        // Get the info we need to draw.
        List<Image> imgBoard = gState.imgBoard();
        List<Image> imgBombsExplosions = gState.imgBombsExplosions();
        List<Image> imgScores = gState.imgScores();
        List<Image> imgTime = gState.imgTime();
        List<GameState.Player> players = new ArrayList<>(gState.players());

        // Draw the board, blasts and bombs.
        // The null in methods represent image observers that we don't use.
        int x = 0;
        int y = 0;
        for (int j = 0; j < Cell.ROWS; j++) {
            for (int i = 0; i < Cell.COLUMNS; i++) {
                Image toDraw = imgBoard.get(j * Cell.COLUMNS + i);
                graphX.drawImage(toDraw, x, y, null);
                graphX.drawImage(imgBombsExplosions.get(j * Cell.COLUMNS + i), x, y, null);
                x += toDraw.getWidth(null);
            }
            y += imgBoard.get(j * Cell.COLUMNS).getHeight(null);
	    x = 0;
        }
        
        // Draw the Players (that we sort with the above created Comparator).
        players.sort(finalComp);
        for(GameState.Player p : players){
            graphX.drawImage(p.img(), FX.apply(p.position().x()), FY.apply(p.position().y()), null);
        }
        
        // Draw the score line.
        for (Image img : imgScores) {
            graphX.drawImage(img, x, y, null);
            x += img.getWidth(null);
        }

	// Let's assume we can't get an empty score line for simplicity's sake.
        y += imgScores.get(0).getHeight(null);
        x = 0;
        
        // Write the text on the scoreboard.
        Font font = new Font("Arial", Font.BOLD, 25);
        graphX.setColor(Color.WHITE);
        graphX.setFont(font);
        for(GameState.Player p : players){
            StringBuilder sBu = new StringBuilder();
            sBu.append(p.lives());
            int xfont, yfont = 659;
            switch (p.id()){
            case PLAYER_1:
                xfont = 96;
                break;
            case PLAYER_2:
                xfont = 240;
                break;
            case PLAYER_3:
                xfont = 768;
                break;
            case PLAYER_4:
                xfont = 912;
                break;
            default:
                // So the xfont got a default value and is initialized.
                xfont = 0;
            };
            
            graphX.drawString(sBu.toString(), xfont, yfont);
        }
        
        
	// Draw the timeline.
        for (Image img : imgTime){
            graphX.drawImage(img, x, y, null);
            x += img.getWidth(null);
        }
	// Once again, let's just assume time can't get empty. FUBAR otherwise.
        y += imgTime.get(0).getHeight(null);
    }

    /**
     * This method updates the state of the game we are drawing.
     *
     * @param gState
     *          The GameState we are drawing.
     * @param pov
     *          Sets who's point of view we will use to draw the game.
     */
    public void setGameState(GameState gState, PlayerID pov) {
        this.gState = gState;
        this.pov = pov;
        repaint();
    }
}
