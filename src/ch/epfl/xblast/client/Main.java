package ch.epfl.xblast.client;

import java.awt.event.KeyEvent;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.StandardProtocolFamily;
import java.nio.channels.DatagramChannel;
import java.nio.ByteBuffer;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.function.Consumer;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.PlayerAction;

/**
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class Main {
    // Class constants.
    private final static int MAX_GAMESTATE_SIZE = 409;
    private final static int serverPort = 2016;

    // Class variables.
    private static XBlastComponent gameJComponent;
    private static DatagramChannel channel;
    private static InetSocketAddress serverAddr = null;
    private static GameState gState = null;
    private static PlayerID pov = null;

    public static void main(String args[]) throws IOException, IllegalArgumentException, InterruptedException, InvocationTargetException {
        // Sanity check on passed arguments.
        if (args.length > 1) {
            System.out.println("Xblast client: invalid number of arguments: " + args.length);
            System.out.println("usage: client [server_address]");
            System.out.println("Will default to localhost if no address is given.");
            System.exit(1);
        } else if (args.length == 1) {
		serverAddr = new InetSocketAddress(args[0], serverPort);
        } else {
		serverAddr = new InetSocketAddress("localhost", serverPort);
        }

	// Check that the given address is correct:
	if (serverAddr.isUnresolved()) {
	    throw new IllegalArgumentException("The server's address cannot be resolved!");
	}

        // Open an IPV4 channel.
        channel = DatagramChannel.open(StandardProtocolFamily.INET);
        channel.configureBlocking(false);

        // Create the ask-to-join message.
        ByteBuffer canHasXBlast = ByteBuffer.allocate(1);
        canHasXBlast.put((byte) 0);
        canHasXBlast.flip();

        // Create the buffer that will contain the server's answers.
        // 1 byte for the player number + the worst compression case.
        ByteBuffer currentStateBuf = ByteBuffer.allocate(1 + MAX_GAMESTATE_SIZE);

        // Phase 1: Asking to join a game.
        while(gState == null) {
            channel.send(canHasXBlast, serverAddr);
	    System.out.println("Requesting join to " + serverAddr.getHostName() + "on port " + serverPort + "...");

            // Wait a second for an answer.
            Thread.sleep(1000);

            // Check for an answer and process it.
            SocketAddress senderAddress = channel.receive(currentStateBuf);
            if (senderAddress != null && senderAddress.equals(serverAddr)) {
                currentStateBuf.flip();
                pov = PlayerID.values()[currentStateBuf.get()];
                gState = gStateProcessing(currentStateBuf);
	    }
        }

        // Clear the buffer for further use.
        currentStateBuf.clear();

        // Create the UI in a second thread
        SwingUtilities.invokeAndWait(() -> createUI());
	
	// Phase 2: As long as the game is not finished, continue.
        // Now set the channel to be blocking.
        channel.configureBlocking(true);

        // Maybe try to be smarter on how we deal with the end-game?
	// We'll see during the bonuses.
	boolean isfinished = false;
	while (!isfinished) {
            channel.receive(currentStateBuf);
            currentStateBuf.flip();
            pov = getPOV(currentStateBuf);
            gState = gStateProcessing(currentStateBuf);
	    gameJComponent.setGameState(gState, pov);
            currentStateBuf.clear();
	}
    }
    
    private static PlayerID getPOV(ByteBuffer givenBuffer) {
        return PlayerID.values()[givenBuffer.get()];
    }

    private static GameState gStateProcessing(ByteBuffer givenBuffer) {
        List<Byte> serializedGameState = new ArrayList<>();
        while(givenBuffer.hasRemaining()) {
            serializedGameState.add(givenBuffer.get());
        }
        return GameStateDeserializer.deserializeGameState(serializedGameState);
    }

    private static void createUI() {
        // Create the swing component for Xblast
        gameJComponent = new XBlastComponent();
        gameJComponent.setGameState(gState, pov);

        // Create the consumer that will be passed to the keylistener.
        Consumer<PlayerAction> sendKeys = p -> {
            ByteBuffer toSend = ByteBuffer.allocate(1);
            toSend.put((byte) p.ordinal());
	    toSend.flip();
	    try {
                channel.send(toSend, serverAddr);
            } catch(IOException e) {
                System.err.println("Something, somewhere, went terribly wrong. Pray to the ponies (they are magic).");
		e.printStackTrace();
            } catch(Exception e) {
                System.err.println("Could not send keypress because of :");
                e.printStackTrace();
            }
        };

        // Create the map of possible actions.
	Map<Integer, PlayerAction> kb = new HashMap<>();
        kb.put(KeyEvent.VK_UP, PlayerAction.MOVE_N);
        kb.put(KeyEvent.VK_RIGHT, PlayerAction.MOVE_E);
        kb.put(KeyEvent.VK_DOWN, PlayerAction.MOVE_S);
        kb.put(KeyEvent.VK_LEFT, PlayerAction.MOVE_W);
        kb.put(KeyEvent.VK_SPACE, PlayerAction.DROP_BOMB);
        kb.put(KeyEvent.VK_SHIFT, PlayerAction.STOP);

        gameJComponent.addKeyListener(new KeyboardEventHandler(kb, sendKeys));

	// Set the root frame.
        JFrame jMain = new JFrame("XBlast");
        jMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        jMain.getContentPane().add(gameJComponent);
        jMain.pack();
        jMain.setVisible(true);

	// Request keyboard focus to capture input.
        gameJComponent.requestFocusInWindow();
    }
}
