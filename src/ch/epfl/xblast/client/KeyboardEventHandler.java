package ch.epfl.xblast.client;

import ch.epfl.xblast.PlayerAction;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Map;
import java.util.HashMap;
import java.util.function.Consumer;
import java.util.Collections;
import java.util.Objects;

/**
 * Handle human player keyboard events.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class KeyboardEventHandler extends KeyAdapter implements KeyListener {
    // We keep a table of actions and a consumer that acts upon them.
    private final Map<Integer, PlayerAction> plActions;
    private final Consumer<PlayerAction> plAct;

    /**
     * Build a new handler based on a given map of actions and what to do with them.
     *
     * @param pActions
     *          A map of integers to actions, the integer being the corresponding keyCode.
     * @param nomNom
     *          A consumer that will eat up the actions we are passed.
     */
    public KeyboardEventHandler(Map<Integer, PlayerAction> pActions, Consumer<PlayerAction> nomNom) {
        plActions = new HashMap<Integer, PlayerAction>(Collections.unmodifiableMap(Objects.requireNonNull(pActions)));
        plAct = nomNom;
    }

    /**
     * Intercepts keypresses and fires the associated action if it exists.
     * @param kEvent
     *          The event that we should handle.
     */
    @Override
    public void keyPressed(KeyEvent kEvent) {
        if (plActions.containsKey(kEvent.getKeyCode())) {
            plAct.accept(plActions.get(kEvent.getKeyCode()));
        }
    }
}
