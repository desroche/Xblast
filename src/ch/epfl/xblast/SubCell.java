package ch.epfl.xblast;

/**
 * Smaller sections of the board that allow fine-grained control over a Player's movements.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class SubCell {
    // Class constants
    private static final int COLUMNS = 240;
    private static final int ROWS = 208;
    private static final int CELL_SIZE = 16;
    private static final int CELL_CENTRAL = 8;

    // The subcell's horizontal coordinate.
    private final int x;

    // The subcell's vertical coordinate.
    private final int y;

    /**
     * Build a subcell based on it's position on the board.
     *
     * @param x
     *            The subcell's horizontal coordinate. Any value is accepted,
     *            but the values will be normalized between 0 and 239.
     * @param y
     *            The subcell's vertical coordinate. Any value is accepted, but
     *            the values will be normalized between 0 and 207.
     */
    public SubCell(int x, int y) {
        // The passed values are normalized to fit on the board.
        this.x = Math.floorMod(x, COLUMNS);
        this.y = Math.floorMod(y, ROWS);
    }

    /**
     * This method is used to find the subcell of a given cell.
     *
     * @param cell
     *            The Cell that we want to find the central subcell of.
     * @return The Cell's central subcell.
     * @see ch.epfl.xblast.Cell
     */
    public static SubCell centralSubCellOf(Cell cell) {
        // A cell's central subcell has (8,8) as it's coordinates in the cell's
        // referential. As such, we must first find the cell's coordinates, and
	// compute the central subcell from that.
        int subCentralX = ((cell.x() * CELL_SIZE) + CELL_CENTRAL);
        int subCentralY = ((cell.y() * CELL_SIZE) + CELL_CENTRAL);
        return new SubCell(subCentralX, subCentralY);
    }

    /**
     * Get the subcell's horizontal coordinate.
     * 
     * @return Origin's horizontal coordinate.
     */
    public int x() {
        return x;
    }

    /**
     * Get the subcell's vertical coordinate.
     *
     * @return Origin's vertical coordinate.
     */
    public int y() {
        return y;
    }

    /**
     * Calculates the
     * <a href="https://en.wiktionary.org/wiki/Manhattan_distance">Manhatta Distance</a>
     * between origin and it's Cell's central subcell.
     *
     * @return The Manhattan distance between origin and it's Cell central
     *         subcell.
     */
    public int distanceToCentral() {
        if (this.isCentral()) {
            return 0;
        } else {
            SubCell central = centralSubCellOf(this.containingCell());
            return (Math.abs(x - central.x()) + Math.abs(y - central.y()));
        }
    }

    /**
     * Checks if origin is it's Cell's central subcell.
     *
     * @return True if origin is it's Cell's central subcell, False otherwise.
     */
    public boolean isCentral() {
        return ((x - CELL_CENTRAL) % CELL_SIZE == 0 && (y - CELL_CENTRAL) % CELL_SIZE == 0);
    }

    /**
     * Find the neighbor of origin in a given direction.
     *
     * @param d
     *            The direction to look for origin's neighbor.
     * @return The subcell neighbor of origin in the given direction.
     * @see ch.epfl.xblast.Direction
     */
    public SubCell neighbor(Direction d) throws IllegalArgumentException {
        switch (d) {
        case N:
            return new SubCell(x, (y - 1));
        case E:
            return new SubCell((x + 1), y);
        case S:
            return new SubCell(x, (y + 1));
        case W:
            return new SubCell((x - 1), y);
	// Just in case the matrix segfaults or what
        default:
            throw new IllegalArgumentException("Unknown direction");
        }
    }

    /**
     * Find origin's containing cell.
     *
     * @return The Cell that contains origin.
     * @see ch.epfl.xblast.Cell
     */
    public Cell containingCell() {
        return new Cell((x / CELL_SIZE), (y / CELL_SIZE));
    }

    /**
     * Override Object's equal method to use position to check if origin and
     * another subcell are equal.
     *
     * @param that
     *            An Object that will be compared with origin for equality.
     * @return True if the argument is a Subcell and has the same position as
     *         origin.
     */
    @Override
    public boolean equals(Object that) {
        return (that != null 
                && (that instanceof SubCell)
                && (((SubCell)that).x() == this.x 
                && ((SubCell)that).y() == this.y));
    }

    /**
     * Override Object's toString method to print the SubCell as it's
     * coordinates.
     *
     * @return Origin's position in the (x,y) format.
     */
    @Override
    public String toString() {
        return ("(" + x + "," + y + ")");
    }
    
    /**
     * Override Object's hashcode method to give a better hashcode for each SubCell (Row major index position)
     * 
     * @return a unique hashcode for each different subcell.
     */
    @Override
    public int hashCode(){
        return x + y * COLUMNS;
    }
}
