package ch.epfl.xblast;

/**
 * Implements different checks, like if an int is positive,
 * etc...
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */

public final class ArgumentChecker {

    // ArgumentChecker is not instanciable.
    private ArgumentChecker() {}

    /**
     * Check that the given int is positive.
     * 
     * @param value
     *            The value we want to check.
     * @return The value if it is greater than or equal to zero.
     * @throws IllegalArgumentException
     *             If the value passed as a parameter is not positive.
     */
    public static int requireNonNegative(int value)
            throws IllegalArgumentException {
        if (value >= 0) {
            return value;
        } else {
            throw new IllegalArgumentException("You passed a negative int!");
        }
    }

    /**
     * Check that the given byte is positive in 2-complement notation.
     *
     * @param value
     * 		The value we are checking.
     * @return The passed parameter if it is greater than or equal to zero.
     * @throws IllegalArgumentException
     * 		If the passed parameter is strictly negative.
     */
    public static byte requireNonNegative(byte value) throws IllegalArgumentException {
        if (value >= 0) {
            return value;
        } else {
            throw new IllegalArgumentException("You passed a negative byte!");
        }
    }
}
