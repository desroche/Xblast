package ch.epfl.xblast;

/**
 * This enumeration keeps track of the different actions a human player can do.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public enum PlayerAction {
	JOIN_GAME, MOVE_N, MOVE_E, MOVE_S, MOVE_W, STOP, DROP_BOMB;
}
