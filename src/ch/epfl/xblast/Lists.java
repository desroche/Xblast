package ch.epfl.xblast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Final class that cannot be instanciated (yet).
 *
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class Lists {
    
    private Lists() {
    }

    /**
     * A static method that return a symmetric version of the List passed as a 
     * parameter.
     *
     * @param l
     *            The list we want to reverse.
     * @param <T>
     *            The type of the objects contained in the list.
     * @throws IllegalArgumentException
     *             Throws this exception if the List given is null or if it's
     *             empty.
     * @return A symmetric version of the list passed as a parameter.
     */
    public static <T> List<T> mirrored(List<T> l)
            throws IllegalArgumentException {
        if (l == null || l.isEmpty()) {
            throw new IllegalArgumentException(
                    "The passed list is empty or null!");
        } else {

            // The symmetric of a one-element list is itself.
            if (l.size() == 1) {
                return l;
            } else {
                // If the list has more than one element, we have to do a copy of
                // itself except the last element, reverse it, and add it to
                // the new List.
                List<T> listRet = new ArrayList<T>();
                listRet.addAll(l);
                List<T> subl = new ArrayList<>(
                        l.subList(0, listRet.size() - 1));
                Collections.reverse(subl);
                listRet.addAll(subl);
                return listRet;
            }
        }
    }

    /**
     * A static method that returns a list of the different permutations of 
     * the elements of the list passed as a parameter.
     * 
     * @param l
     *            The List we want to find permutations for.
     * @param <T>
     *            The type of the objects contained in the list.
     *
     * @return  A List of the different permutations of the list passed as a parameter.
     */
    public static <T> List<List<T>> permutations(List<T> l) {
        List<List<T>> toRet = new ArrayList<>();
	// Base of our recursive function
        if (l.isEmpty()) {
            toRet.add(new ArrayList<>());
            return toRet;
	// Body
        } else {
            List<T> tail;
            if( l.size() > 1 ){
                tail = new ArrayList<>(l.subList(1, l.size()));
            }else{
                toRet.add(l);
                return toRet;
            }
            
            T head = l.get(0);
	    // Recursively get the permutations for the list's tail
            List<List<T>> litPerm = permutations(tail);

            // We input in each of the permutations at each position the first
            // element T of l.
            for (List<T> listT : litPerm) {
                for (int j = 0; j <= listT.size(); ++j) {
                    List<T> semiRet = new ArrayList<>(listT);
                    semiRet.add(j, head);
                    toRet.add(semiRet);
                }
            }
            return toRet;
        }
    }
}
