package ch.epfl.xblast;

import ch.epfl.xblast.server.Ticks;

/**
 * This interface is used to keep track of universal constants.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 * @see Ticks
 */

public interface Time {

    /**
     * Number of seconds in a minute in the current snapshot of our Universe.
     */
    public static final int S_PER_MIN = 60;

    /**
     * Number of milliseconds per second in our area of space-time.
     */
    public static final int MS_PER_S = 1000;

    /**
     * Number of microseconds per second in our quantic positionning.
     */
    public static final int US_PER_S = 1000 * MS_PER_S;

    /**
     * Number of nanoseconds per second depending on the planets' alignement's...
     */
    public static final int NS_PER_S = 1000 * US_PER_S;

}
