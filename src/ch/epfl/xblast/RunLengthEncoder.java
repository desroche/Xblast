package ch.epfl.xblast;

import java.util.List;
import java.util.ArrayList;

/**
 * This class encodes and decodes lists of bytes using a modified form of run length encoding.
 * 
 * @author Joachim Desroches (257178)
 * @author Benoit Juif (261829)
 */
public final class RunLengthEncoder {
    // Maximum number of repetitions that can be compressed in two bytes using our runlength encoding.
    // 128 of one byte + 2 by our convention.
    private final static int MAX_RUN_LENGTH = 130;
    // As this class only has static methods, we don't want it to be instanciated.
    private RunLengthEncoder() {}

    /**
     * This method encodes a sequence of bytes using run length encoding.
     *
     * @param clearText
     *     The list of bytes that we want to compress using run length encoding.
     * @return The input compressed using run length encoding.
     */
    public static List<Byte> encoder(List<Byte> clearText) {
        List<Byte> output = new ArrayList<>();

        // create later constants. 
        int acc = -1;
        Byte lastSeen = 0b00000000;

        for (Byte symb : clearText) {
            // detect first iteration to initialize values.
            if (acc < 0) {
                acc = 1;
                lastSeen = ArgumentChecker.requireNonNegative(symb);
            // normal processing
            } else {
                // Just count if we have the same byte as last time, except if we arrive to our maximum value. 
                if (ArgumentChecker.requireNonNegative(symb) == lastSeen && acc < MAX_RUN_LENGTH) {
                    acc++;
                } else {
                    // Add the byte
                    output = adder(output, acc, lastSeen);
                    // Start counting at one again.
                    acc = 1;
                    lastSeen = symb;
                }
            }
        }

	// Because we use an iterator, we need to take care of the end of the list:
	output = adder(output, acc, lastSeen);
        return output;
    }

    /**
     * This method takes a sequence of bytes compressed using run length encoding and decompresses it.
     *
     * @param cipherText
     * 		the list of bytes to decde.
     * @return The decoded list of bytes.
     */
    public static List<Byte> decoder(List<Byte> cipherText) {
        List<Byte> output = new ArrayList<>();

        // used to check that the last byte seen is not negative.
        Byte lastSeen = 0b00000000;

        for (Byte symb : cipherText) {
            if ((int)symb >= 0) {
                if ((int)lastSeen < 0) {
                    for (int i = 2; i > (int)lastSeen; i--) {
                        output.add(symb);
                    }
                } else {
                    output.add(symb);
                }
            }
            lastSeen = symb;
        }
        if ((int)lastSeen < 0) {
            throw new IllegalArgumentException("The last byte of the sequence to decode cannot be negative in 2-complement notation!");
        }
        return output;
    }

    /*
     * Helper method to add bytes to a list for run length.
     */
    private static List<Byte> adder(List<Byte> output, int acc, Byte lastSeen) {
        // To make sure to minimize space, if we have less than three repetitions,
        // we add them as is in the encoded list.
        if (acc < 3) {
            for (int i = 0; i < acc; i++) {
                output.add(lastSeen);
            }
        // If, however, we have three or more, than we use run length.
        } else {
            // Add the correct values.
            output.add(new Byte((byte)(2 - acc)));
            output.add(lastSeen);
        }
        return output;
    }
}
